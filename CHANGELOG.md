## 1.3.14

- deleteGroupWithDevicesError exception

## 1.3.13

- internal/devices/ID/connectivity -> v2/devices/ID/connectivity NOT_PROVISIONED error

## 1.3.12

- fix when get me fail when session presenter start

## 1.3.11

- deviceExistsForWorkspace devices presenter
- getInternalDevice api provider

## 1.3.10

- menu in user object + session stream with user instead of status

## 1.3.9

- workspace settings logoApp - fix fetch messages

## 1.3.8

- Device warning cause - ice

## 1.3.7

- GetConnectivityMessagesRequest with device_to_replace_id query param

## 1.3.5

- Maintenance with comment
- owner_email when creating group

## 1.3.4

- DeviceOperationType newTapping -> reinstallSameLeg

## 1.3.3

- Maintenances for setup silos

## 1.3.2

- Device isSilo property

## 1.3.1

- change route for get me roles
- change route for get front config

## 1.3.0

- fetchIBCModels service
- validateDeviceIBC service
- DevicePayload object
- PaginatedQueryParam rename to QueryParam
- QueryParam with CustomQueryParam list
- maintainingDevice service

## 1.2.15

- Presenter function fetchDevicesInstalledPaginated (provider fetchDevicesPaginated)
- PaginatedQueryParam with ordering property
- Device properties isTank & model & color

## 1.2.14

- return rowCount for Paginated service
- devicesCombined & devicesNotCombined getter in Device model

## 1.2.13

- fetchGroupsPaginated & fetchGroupsPoiPaginated
- fix DeviceContentType & DeviceContent parsing

## 1.2.12

- Remove role in workspace object and use groupIds to invite

## 1.2.11

- Role with isGroupRoleDelegated, hierarchy and dedicatedToProduct

## 1.2.10

- GroupPoi property in Device object

## 1.2.9

- Error security 401 workspaceMismatch

## 1.2.8

- Comment property in Group object

## 1.2.7

- Fix role parsing for user model

## 1.2.6

- Role displayName
- Device DeviceBatteryStatus metadata mapping

## 1.2.5

- Role isGlobal

## 1.2.4

- Device DeviceBatteryStatus

## 1.2.0

- use paginated WS + remove useless WS and class (ObjectWithLabel, ObjectSelected)
- error deliveryAlreadyExistForSiloDate
- GetWorkspacesRequest
- IUserPresenter
- getLanguages
- User (CRUD)
- Device OperationSuggested
- Dashboard filters by status
- CustomView (CRUD)
- Calibrations (GET)

## 1.1.51

- roleV2
- edit group
- edit location
- rename site to group

## 1.1.50

- repeaters in connectivity messages

## 1.1.49

- secure orderDatesDisabled date format

## 1.1.48

- orderNotFound and deviceNotFound exception logic

## 1.1.47

- ws register body with is_terms_accepted true

## 1.1.46

- ws to validate register token and register new account

## 1.1.45

- workspace settings orderDatesDisabled and orderWeekdaysEnabled for order logic

## 1.1.44

- get terms and conditions url service (mock)

## 1.1.43

- front config ws with min_version

## 1.1.42

- handle FlutterSecureStorage exceptions

## 1.1.41

- handle FlutterSecureStorage exceptions

## 1.1.40

- wrongOldPassword & wrongNewPassword exceptions

## 1.1.39

- BinconnectV2 to manage settingV2 workspace setting

## 1.1.38

- remove fillingUnitToDisplayShort & fillingUnitToDisplayLong

## 1.1.37

- settings fillingUnit

## 1.1.36

- bug fix bool.tryParse again

## 1.1.35

- bug fix bool.tryParse again

## 1.1.34

- bug fix bool.tryParse

## 1.1.33

- use bool.tryParse static method

## 1.1.32

- disableOrders workspace settings

## 1.1.31

- levelHistory (DataPointValue & DataPointLevel for graphData)

## 1.1.30

- exception tooManyMedia & fileTooLarge

## 1.1.29

- rename siloFillingUnitToDisplayShort & siloFillingUnitToDisplayLong to fillingUnitToDisplayShort & fillingUnitToDisplayLong

## 1.1.28

- settings siloFillingUnitToDisplayShort & siloFillingUnitToDisplayLong

## 1.1.27

- configuration in user object

## 1.1.26

- get daily consumption function

## 1.1.25

- flutter_lints 3.0.0

## 1.1.24

- get graph data in data layer refacto

## 1.1.23

- reinstall WS with clearHistory and operationType param

## 1.1.22

- support antenna rssi in string too

## 1.1.21

- return ADC in get connectivity

## 1.1.20

- lib upgrade

## 1.1.19

- save filters in local storage

## 1.1.18

- calibrationAlreadyExistForSiloDate exception

## 1.1.17

- reinstall device WS

## 1.1.16

- filter device with parent combined device

## 1.1.15

- device combined in dashboard v2 WS

## 1.1.14

- error 403 dashboardForbidden for orders

## 1.1.13

- Dasboard v2 error 403 dashboardForbidden

## 1.1.12

- Dasboard v2 new filters levelTonsLessThan levelTonsGreaterThan missingWeightLessThan missingWeightGreaterThan
- Device max_15days and mean_15days for SiloConnect

## 1.1.11

- Fix mock for integration tests

## 1.1.10

- Error unauthorizedWorkspace for GET workspaceSettings

## 1.1.9

- Workspace setting order_suggestion_threshold_tons and dependencies upgrade (intl any, ...)

## 1.1.8

- Remove loginWithWorkspace and add trappedInIce ProblemCause

## 1.1.7

- ErrorMessage object for order_already_exist_for_silo_date

## 1.1.6

- expose ws get user by id in session presenter

## 1.1.5

- reinvite ws + logic in invite

## 1.1.4

- refacto ws post invite

## 1.1.3

- ws device connectivity change route and add status property

## 1.1.2

- ws post calibration level with date in parameter

## 1.1.1

- login without workspace
- ws get me

## 1.0.1

- fix infosToCalibrate parsing for missing_delivery value

## 1.0.0

- Dio 5.0.0
- firstWhereOrNull in extension
- whatsapp_link
- dashboard filters
- infoToCalibrate

## 0.0.2

- BinConnect - SiloConnect - TankConnect - MyTR - Siloman

## 0.0.1

- Setup Silo support
