import 'dart:io';
import 'dart:ui';

import 'package:flutter_nanolike_sdk/data/api/requests/payload/device_payload.dart';
import 'package:flutter_nanolike_sdk/domain/models/accessory.dart';
import 'package:flutter_nanolike_sdk/domain/models/calibration.dart';
import 'package:flutter_nanolike_sdk/domain/models/connectivity_message.dart';
import 'package:flutter_nanolike_sdk/domain/models/custom_view.dart';
import 'package:flutter_nanolike_sdk/domain/models/device_content.dart';
import 'package:flutter_nanolike_sdk/domain/models/device_content_type.dart';
import 'package:flutter_nanolike_sdk/domain/models/device.dart';
import 'package:flutter_nanolike_sdk/domain/models/device_operation_type.dart';
import 'package:flutter_nanolike_sdk/domain/models/drug.dart';
import 'package:flutter_nanolike_sdk/domain/models/filters.dart';
import 'package:flutter_nanolike_sdk/domain/models/front_config.dart';
import 'package:flutter_nanolike_sdk/domain/models/graph_data.dart';
import 'package:flutter_nanolike_sdk/domain/models/group_membership.dart';
import 'package:flutter_nanolike_sdk/domain/models/ibc_model.dart';
import 'package:flutter_nanolike_sdk/domain/models/media.dart';
import 'package:flutter_nanolike_sdk/domain/models/order.dart';
import 'package:flutter_nanolike_sdk/domain/models/paginated_data.dart';
import 'package:flutter_nanolike_sdk/domain/models/group.dart';
import 'package:flutter_nanolike_sdk/domain/models/query_param.dart';
import 'package:flutter_nanolike_sdk/domain/models/report.dart';
import 'package:flutter_nanolike_sdk/domain/models/supplement.dart';
import 'package:flutter_nanolike_sdk/domain/models/token.dart';
import 'package:flutter_nanolike_sdk/domain/models/user.dart';
import 'package:flutter_nanolike_sdk/domain/models/role.dart';
import 'package:flutter_nanolike_sdk/domain/models/workspace.dart';
import 'package:flutter_nanolike_sdk/domain/models/workspace_settings.dart';

abstract class IWebApiProvider {
  Future<JWTToken> signIn(
      {required String login, required String password, String? workspace});

  Future<void> resetPassword(String email, String language);

  Future<String> validateRegisterToken(String token);

  Future<void> register(String token, String password);

  Future<PaginatedData<IBCModel>> fetchIBCModels(
      {required QueryParam queryParam});

  Future<User> updateUser(
      {required String userId,
      String? firstName,
      String? lastName,
      String? oldPassword,
      String? newPassword,
      bool? notificationPush,
      bool? notificationEmail,
      Locale? preferredLanguage,
      bool? displayForecast,
      bool? displayRemaining,
      bool? displayLastOrder,
      bool? displayQuickActionOrder,
      String? roleId});

  Future<void> deleteAccount(String userId);

  Future<PaginatedData<Group>> fetchGroupsPaginated(
      QueryParam params, bool isPoi);

  Future<PaginatedData<Device>> fetchDevicesPaginated(QueryParam params);

  Future<Group> fetchGroup(String groupId);

  Future<List<Device>> fetchGroupDevices(String groupId);

  Future<PaginatedData<Device>> fetchDashboard(
      {required QueryParam queryParam, Filters? filters});

  Future<Device?> fetchDashboardDevice({required String deviceId});

  Future<Report> validateDeviceIBC({required String deviceReference});

  Future<List<GraphData>> getGraphData(
      {List<String>? deviceIds,
      List<String>? groupIds,
      required List<String> dataTypes,
      required bool isLastValue,
      DateTime? from,
      DateTime? to});

  Future<List<Accessory>> fetchGroupAccessories(String groupId);

  Future<Accessory> createAccessory(
      {required String groupId,
      required String accessoryId,
      required AccessoryType accessoryType});

  Future<void> deleteAccessory(int accessoryId);

  Future<Group> createGroup(
      final String name,
      final String? customId,
      final String? postalAddress,
      final String? zipCode,
      final String? town,
      final String? comment,
      final String? ownerEmail,
      final List<String> orderRecipients);
  Future<Group> updateGroup(
    final String groupId,
    final String? name,
    final String? customId,
    final String? postalAddress,
    final String? zipCode,
    final String? town,
    final String? comment,
  );

  Future<Device> createDevice({required DevicePayload devicePayload});

  Future<Device> updateDevice(
      {required String deviceReference, required DevicePayload devicePayload});

  Future<void> deleteDevice(String deviceId);

  Future<Device> getDevice(String deviceId);

  Future<void> deleteGroup(String groupId);

  Future<void> createCalibrationLevel(
      {required String deviceId, required num level, DateTime? date});

  Future<List<Order>> getOrders();

  Future<Order> getOrder(String orderId);

  Future<void> createOrder(Order order);

  Future<void> updateOrder(Order order);

  Future<PaginatedData<DeviceContent>> getDeviceContentsPaginated(
      QueryParam params);

  Future<PaginatedData<DeviceContentType>> getDeviceContentTypesPaginated(
      QueryParam params);

  Future<PaginatedData<Drug>> getDrugsPaginated(QueryParam params);

  Future<PaginatedData<Supplement>> getSupplementsPaginated(QueryParam params);

  Future<void> createDeviceContent(String deviceContent);

  Future<void> reinstallDevice(
      {required String deviceToMaintainReference,
      required String newDeviceReference,
      required DeviceOperationType deviceOperationType});

  Future<void> maintainingDevice(
      {required String deviceToMaintainReference,
      required DeviceOperationType deviceOperationType,
      String? comment});

  Future<List<Role>> getUserRoles();

  Future<List<Role>> getGroupRoles();

  Future<User> invite(
      {required String email,
      String? firstName,
      String? lastName,
      required List<String> groupIds,
      required Role role,
      required Locale locale});

  Future<User> reinvite({required String userId});

  Future<User> getUser(String userId);

  Future<PaginatedData<User>> getUsersPaginated(QueryParam params);

  Future<User> getMe();

  Future<List<Workspace>> getWorkspaces();

  Future<PaginatedData<Calibration>> getCalibrations(
      {required QueryParam params,
      String? deviceReference,
      CalibrationType? type,
      DateTime? start,
      DateTime? end,
      String? groupId});

  Future<List<Role>> getMeRoles();

  Future<PaginatedData<Role>> getMeRolesPaginated(QueryParam params);

  List<Locale> getLanguages();

  Future<FrontConfig> getFrontConfig();

  Future<String> getTermsAndConditions(String language);

  Future<void> addUserToGroup({required User user, required Group group});

  Future<void> deleteUserFromGroupMembership(
      {required User user, required GroupMembership groupMembership});

  Future<List<ConnectivityMessage>> fetchConnectivityMessages(
      {required String deviceReference, String? deviceToMaintainReference});

  Future<List<Media>> getDeviceMedias(String deviceId);

  Future<void> createDeviceMedia(String deviceId, File media);

  Future<void> deleteDeviceMedia(String mediaId);

  Future<void> addFirebaseToken(String userId, String token);

  Future<void> deleteFirebaseToken(String userId, String token);

  Future<WorkspaceSettings> workspaceSettings();

  void updateWorkspaceHeader(String? workspace);

  Future<PaginatedData<CustomView>> getCustomViewsPaginated(QueryParam params);

  Future<CustomView> getCustomView(String customViewId);

  Future<CustomView> createCustomView(String name, Filters filters);

  Future<CustomView> updateCustomView(
      String customViewId, String name, Filters filters);

  Future<void> deleteCustomView(String customViewId);
}

abstract class IWebApiJWTProvider extends IWebApiProvider {
  Stream<JWTToken?> get jwtTokenStream;
  void logout();
}
