import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/payload/device_payload.dart';
import 'package:flutter_nanolike_sdk/domain/models/accessory.dart';
import 'package:flutter_nanolike_sdk/domain/models/calibration.dart';
import 'package:flutter_nanolike_sdk/domain/models/device.dart';
import 'package:flutter_nanolike_sdk/domain/models/device_operation_type.dart';
import 'package:flutter_nanolike_sdk/domain/models/error_response.dart';
import 'package:flutter_nanolike_sdk/domain/models/filters.dart';
import 'package:flutter_nanolike_sdk/domain/models/graph_data.dart';
import 'package:flutter_nanolike_sdk/domain/models/ibc_model.dart';
import 'package:flutter_nanolike_sdk/domain/models/media.dart';
import 'package:flutter_nanolike_sdk/domain/models/nanolike_exception.dart';
import 'package:flutter_nanolike_sdk/domain/models/order.dart';
import 'package:flutter_nanolike_sdk/domain/models/paginated_data.dart';
import 'package:flutter_nanolike_sdk/domain/models/group.dart';
import 'package:flutter_nanolike_sdk/domain/models/query_param.dart';
import 'package:flutter_nanolike_sdk/domain/models/report.dart';
import 'package:flutter_nanolike_sdk/domain/presenters/devices/devices_presenter.dart';
import 'package:flutter_nanolike_sdk/domain/providers/api_provider.dart';
import 'package:flutter_nanolike_sdk/helpers/extensions.dart';

class DevicesPresenterImplementation implements IDevicesPresenter {
  final IWebApiJWTProvider _apiProvider;

  DevicesPresenterImplementation(this._apiProvider);

  @override
  Future<PaginatedData<Device>> dashboard(
      {required QueryParam queryParam, Filters? filters}) async {
    try {
      return await _apiProvider.fetchDashboard(
          queryParam: queryParam, filters: filters);
    } catch (error) {
      if (error is DioException && error.response?.statusCode == 403) {
        throw NanolikeException(type: NanolikeExceptionType.dashboardForbidden);
      }
      throw NanolikeException(type: NanolikeExceptionType.other);
    }
  }

  @override
  Future<PaginatedData<Device>> fetchDevicesInstalledPaginated(
      QueryParam queryParam) async {
    return await _apiProvider.fetchDevicesPaginated(QueryParam(
        page: queryParam.page,
        pageSize: queryParam.pageSize,
        search: queryParam.search,
        ordering: '-device_install_date'));
  }

  @override
  Future<Device> fetchDevice({required String deviceId}) async {
    final Device? device =
        await _apiProvider.fetchDashboardDevice(deviceId: deviceId);
    return device ??
        (throw NanolikeException(type: NanolikeExceptionType.deviceNotFound));
  }

  @override
  Future<List<Device>> fetchGroupDevices(String groupId) async {
    return await _apiProvider.fetchGroupDevices(groupId);
  }

  @override
  Future<List<Device>> fetchDevicesWithGraphDataForGroups(
      List<Group> groups) async {
    if (groups.isEmpty) return [];
    final devices = groups.expand((group) => group.devices).toList();
    if (devices.isEmpty) return [];
    final graphs = await _apiProvider.getGraphData(
        groupIds: groups.map((e) => e.id).toList(),
        isLastValue: true,
        //TODO: use "level" and "levelPredictions" instead and inject missingWeight in levelPredictions data_types
        dataTypes: const [
          "level_t",
          "levelJPlus1T",
          "levelJPlus2T",
          "levelJPlus3T",
          "levelJPlus4T",
          "level_percent",
          "levelJPlus1Percent",
          "levelJPlus2Percent",
          "levelJPlus3Percent",
          "levelJPlus4Percent",
          "missingWeight",
          "missingWeightJPlus1",
          "missingWeightJPlus2",
          "missingWeightJPlus3",
          "missingWeightJPlus4",
        ]);
    final List<Device> devicesWithGraphData = [];
    for (var device in devices) {
      final graph = graphs.where((data) => data.deviceId == device.id).toList();

      /*
      * Level J0
      */

      // Level liter
      final graphLevelLiter = graph.where((e) => e.dataType == "level_liter");
      // Level percent
      final graphLevelPercent =
          graph.where((e) => e.dataType == "level_percent");
      // Level ton
      final graphLevelTon = graph.where((e) => e.dataType == "level_t");
      // Temperature
      final graphTemperature = graph.where((e) => e.dataType == "temperature");
      // Missing weight
      final graphMissingWeight =
          graph.where((e) => e.dataType == "missingWeight");

      final levelJ0 = DataPointLevel(
          date: graphLevelLiter.firstOrNull?.dataPoints?.firstOrNull?.date ??
              DateTime.now(),
          levelL: (graphLevelLiter.firstOrNull?.dataPoints?.firstOrNull
                  as DataPointValue?)
              ?.value,
          levelPercent: (graphLevelPercent.firstOrNull?.dataPoints?.firstOrNull
                  as DataPointValue?)
              ?.value,
          levelT: (graphLevelTon.firstOrNull?.dataPoints?.firstOrNull
                  as DataPointValue?)
              ?.value,
          temperature: (graphTemperature.firstOrNull?.dataPoints?.firstOrNull
                  as DataPointValue?)
              ?.value,
          missingWeight: (graphMissingWeight
                  .firstOrNull?.dataPoints?.firstOrNull as DataPointValue?)
              ?.value);

      /*
      * Level J1
      */

      // Level percent
      final graphLevelPercentJPlus1 =
          graph.where((e) => e.dataType == "levelJPlus1Percent");
      // Level ton
      final graphLevelTonJPlus1 =
          graph.where((e) => e.dataType == "levelJPlus1T");
      // Missing weight
      final graphMissingWeightJPlus1 =
          graph.where((e) => e.dataType == "missingWeightJPlus1");

      final levelJPlus1 = DataPointLevel(
          date: graphLevelPercentJPlus1
                  .firstOrNull?.dataPoints?.firstOrNull?.date ??
              DateTime.now(),
          levelPercent: (graphLevelPercentJPlus1
                  .firstOrNull?.dataPoints?.firstOrNull as DataPointValue?)
              ?.value,
          levelT: (graphLevelTonJPlus1.firstOrNull?.dataPoints?.firstOrNull
                  as DataPointValue?)
              ?.value,
          missingWeight: (graphMissingWeightJPlus1
                  .firstOrNull?.dataPoints?.firstOrNull as DataPointValue?)
              ?.value);

      /*
      * Level J2
      */

      // Level percent
      final graphLevelPercentJPlus2 =
          graph.where((e) => e.dataType == "levelJPlus2Percent");
      // Level ton
      final graphLevelTonJPlus2 =
          graph.where((e) => e.dataType == "levelJPlus2T");
      // Missing weight
      final graphMissingWeightJPlus2 =
          graph.where((e) => e.dataType == "missingWeightJPlus2");

      final levelJPlus2 = DataPointLevel(
          date: graphLevelPercentJPlus2
                  .firstOrNull?.dataPoints?.firstOrNull?.date ??
              DateTime.now(),
          levelPercent: (graphLevelPercentJPlus2
                  .firstOrNull?.dataPoints?.firstOrNull as DataPointValue?)
              ?.value,
          levelT: (graphLevelTonJPlus2.firstOrNull?.dataPoints?.firstOrNull
                  as DataPointValue?)
              ?.value,
          missingWeight: (graphMissingWeightJPlus2
                  .firstOrNull?.dataPoints?.firstOrNull as DataPointValue?)
              ?.value);

      /*
      * Level J3
      */

      // Level percent
      final graphLevelPercentJPlus3 =
          graph.where((e) => e.dataType == "levelJPlus3Percent");
      // Level ton
      final graphLevelTonJPlus3 =
          graph.where((e) => e.dataType == "levelJPlus3T");
      // Missing weight
      final graphMissingWeightJPlus3 =
          graph.where((e) => e.dataType == "missingWeightJPlus3");

      final levelJPlus3 = DataPointLevel(
          date: graphLevelPercentJPlus3
                  .firstOrNull?.dataPoints?.firstOrNull?.date ??
              DateTime.now(),
          levelPercent: (graphLevelPercentJPlus3
                  .firstOrNull?.dataPoints?.firstOrNull as DataPointValue?)
              ?.value,
          levelT: (graphLevelTonJPlus3.firstOrNull?.dataPoints?.firstOrNull
                  as DataPointValue?)
              ?.value,
          missingWeight: (graphMissingWeightJPlus3
                  .firstOrNull?.dataPoints?.firstOrNull as DataPointValue?)
              ?.value);

      /*
      * Level J4
      */

      // Level percent
      final graphLevelPercentJPlus4 =
          graph.where((e) => e.dataType == "levelJPlus4Percent");
      // Level ton
      final graphLevelTonJPlus4 =
          graph.where((e) => e.dataType == "levelJPlus4T");
      // Missing weight
      final graphMissingWeightJPlus4 =
          graph.where((e) => e.dataType == "missingWeightJPlus4");

      final levelJPlus4 = DataPointLevel(
          date: graphLevelPercentJPlus4
                  .firstOrNull?.dataPoints?.firstOrNull?.date ??
              DateTime.now(),
          levelPercent: (graphLevelPercentJPlus4
                  .firstOrNull?.dataPoints?.firstOrNull as DataPointValue?)
              ?.value,
          levelT: (graphLevelTonJPlus4.firstOrNull?.dataPoints?.firstOrNull
                  as DataPointValue?)
              ?.value,
          missingWeight: (graphMissingWeightJPlus4
                  .firstOrNull?.dataPoints?.firstOrNull as DataPointValue?)
              ?.value);

      device.levels = [
        levelJ0,
        levelJPlus1,
        levelJPlus2,
        levelJPlus3,
        levelJPlus4
      ];

      devicesWithGraphData.add(device);
    }
    return devicesWithGraphData;
  }

  @override
  Future<void> deleteDevice({required String deviceReference}) async {
    try {
      await _apiProvider.deleteDevice(deviceReference);
    } catch (error) {
      if (error is DioException && error.response?.statusCode == 403) {
        throw NanolikeException(
            type: NanolikeExceptionType.noPermissionToDeleteDevice);
      }
      throw NanolikeException(
          type: NanolikeExceptionType.deleteDeviceCommonError);
    }
  }

  @override
  Future<Device> getDevice({required String deviceReference}) async {
    return await _apiProvider.getDevice(deviceReference);
  }

  @override
  Future<Accessory?> getAccessory(
      {required String accessoryReference, required String groupId}) async {
    final listAccessories = await _apiProvider.fetchGroupAccessories(groupId);
    final accessoryFound = listAccessories
        .firstWhere((element) => element.sigfoxId == accessoryReference);
    return accessoryFound;
  }

  @override
  Future<Device> createDevice({required DevicePayload devicePayload}) async {
    try {
      return await _apiProvider.createDevice(devicePayload: devicePayload);
    } catch (error) {
      final httpError = error as DioException?;
      final statusCode = httpError?.response?.statusCode;
      final body = httpError?.response?.data.toString().toLowerCase() ?? "";
      switch (statusCode) {
        case 403:
          {
            throw NanolikeException(
                type: NanolikeExceptionType.noPermissionToCreateDevice);
          }
        case 404:
          {
            throw NanolikeException(type: NanolikeExceptionType.deviceNotFound);
          }
        case 400:
          {
            throw NanolikeException(
                type: NanolikeExceptionType.deviceAlreadyExist);
          }
        default:
          {
            if (body.contains("device_reference") &&
                body.contains("already exists")) {
              throw NanolikeException(
                  type: NanolikeExceptionType.deviceAlreadyExist);
            } else if (body.contains("device_reference") &&
                body.contains("does not exist")) {
              throw NanolikeException(
                  type: NanolikeExceptionType.deviceNotFound);
            } else {
              throw NanolikeException(
                  type: NanolikeExceptionType.createDeviceCommonError);
            }
          }
      }
    }
  }

  @override
  Future<Device> updateDevice(
      {required String deviceReference,
      required DevicePayload devicePayload}) async {
    try {
      return await _apiProvider.updateDevice(
          deviceReference: deviceReference, devicePayload: devicePayload);
    } catch (error) {
      final httpError = error as DioException?;
      final statusCode = httpError?.response?.statusCode;
      final body = httpError?.response?.data.toString().toLowerCase() ?? "";
      switch (statusCode) {
        case 403:
          {
            throw NanolikeException(
                type: NanolikeExceptionType.noPermissionToCreateDevice);
          }
        case 404:
          {
            throw NanolikeException(type: NanolikeExceptionType.deviceNotFound);
          }
        case 400:
          {
            throw NanolikeException(
                type: NanolikeExceptionType.deviceAlreadyExist);
          }
        default:
          {
            if (body.contains("device_reference") &&
                body.contains("already exists")) {
              throw NanolikeException(
                  type: NanolikeExceptionType.deviceAlreadyExist);
            } else if (body.contains("device_reference") &&
                body.contains("does not exist")) {
              throw NanolikeException(
                  type: NanolikeExceptionType.deviceNotFound);
            } else {
              throw NanolikeException(
                  type: NanolikeExceptionType.createDeviceCommonError);
            }
          }
      }
    }
  }

  @override
  Future<void> maintainDevice(
      {required String deviceToMaintainReference,
      String? newDeviceReference,
      required DeviceOperationType deviceOperationType,
      String? comment}) async {
    if (deviceOperationType == DeviceOperationType.newDeviceFull ||
        deviceOperationType == DeviceOperationType.newElec) {
      try {
        return await _apiProvider.reinstallDevice(
            deviceToMaintainReference: deviceToMaintainReference,
            newDeviceReference: newDeviceReference ?? "",
            deviceOperationType: deviceOperationType);
      } catch (error) {
        final httpError = error as DioException?;
        final statusCode = httpError?.response?.statusCode;
        switch (statusCode) {
          case 404:
            {
              throw NanolikeException(
                  type: NanolikeExceptionType.deviceNotFound);
            }
          default:
            {
              throw NanolikeException(
                  type: NanolikeExceptionType.createDeviceCommonError);
            }
        }
      }
    } else {
      return await _apiProvider.maintainingDevice(
          deviceToMaintainReference: deviceToMaintainReference,
          deviceOperationType: deviceOperationType,
          comment: comment);
    }
  }

  @override
  Future<void> createCalibrationLevel(
      {required String deviceId, required num level, DateTime? date}) async {
    try {
      await _apiProvider.createCalibrationLevel(
          deviceId: deviceId, level: level, date: date);
    } catch (error) {
      if (error is DioException && error.response?.data != null) {
        if (ErrorResponse.fromJson(error.response?.data).code ==
            "calibration_already_exist_for_silo_date") {
          throw NanolikeException(
              type: NanolikeExceptionType.calibrationAlreadyExistForSiloDate);
        } else if (ErrorResponse.fromJson(error.response?.data).code ==
            "level_quantity_not_valid") {
          throw NanolikeException(
              type: NanolikeExceptionType.levelQuantityNotValid);
        }
      }
      throw NanolikeException(type: NanolikeExceptionType.other);
    }
  }

  @override
  Future<void> createCalibrationDelivery(
      String deviceId, DateTime date, num quantity) async {
    try {
      await _apiProvider.createOrder(Order(
          date: date,
          source: OrderSource.delivery,
          bypassSendEmail: true,
          timeSlot: TimeSlot.morning,
          orderDevices: [
            OrderDevice(deviceReference: deviceId, quantity: quantity)
          ]));
    } catch (error) {
      if (error is DioException && error.response?.data != null) {
        if (ErrorResponse.fromJson(error.response?.data).code ==
            "order_already_exist_for_silo_date") {
          throw NanolikeException(
              type: NanolikeExceptionType.deliveryAlreadyExistForSiloDate);
        } else if (ErrorResponse.fromJson(error.response?.data).code ==
            "delivery_quantity_not_valid") {
          throw NanolikeException(
              type: NanolikeExceptionType.deliveryQuantityNotValid);
        }
      }
      throw NanolikeException(type: NanolikeExceptionType.other);
    }
  }

  @override
  Future<List<Media>> createDeviceMedia(String deviceId, File media) async {
    try {
      await _apiProvider.createDeviceMedia(deviceId, media);
      return await _apiProvider.getDeviceMedias(deviceId);
    } catch (error) {
      if (error is DioException && error.response?.data != null) {
        if (ErrorResponse.fromJson(error.response?.data).code ==
            "TOO_MANY_MEDIAS") {
          throw NanolikeException(type: NanolikeExceptionType.tooManyMedia);
        } else if (ErrorResponse.fromJson(error.response?.data).code ==
            "FILE_TOO_LARGE") {
          throw NanolikeException(type: NanolikeExceptionType.fileTooLarge);
        }
      }
      throw NanolikeException(type: NanolikeExceptionType.other);
    }
  }

  @override
  Future<void> deleteDeviceMedia(String mediaId) async {
    await _apiProvider.deleteDeviceMedia(mediaId);
  }

  @override
  Future<List<Media>> getDeviceMedias(String deviceId) async {
    return await _apiProvider.getDeviceMedias(deviceId);
  }

  @override
  Future<Report> validateDeviceIBC({required String deviceReference}) async {
    return await _apiProvider.validateDeviceIBC(
        deviceReference: deviceReference);
  }

  @override
  Future<PaginatedData<IBCModel>> fetchIBCModels(QueryParam queryParam) async {
    return await _apiProvider.fetchIBCModels(queryParam: queryParam);
  }

  @override
  Future<List<DataPointValue>> dailyConsumption(
      {required String deviceReference,
      required DateTime from,
      required DateTime to}) async {
    final response = await _apiProvider.getGraphData(
        dataTypes: ['daily_analysis'],
        isLastValue: false,
        deviceIds: [deviceReference],
        from: from,
        to: to);
    final data = response.firstOrNull?.dataPoints
            ?.whereType<DataPointValue>()
            .toList() ??
        [];
    // data.removeWhere(
    //     (element) => element.value == null || (element.value is! num));
    return data;
  }

  @override
  Future<List<DataPointLevel>> levelHistory(
      {required String deviceReference,
      required DateTime from,
      required DateTime to}) async {
    final response = await _apiProvider.getGraphData(
        dataTypes: ['level'],
        isLastValue: false,
        deviceIds: [deviceReference],
        from: from,
        to: to);
    final data = response.firstOrNull?.dataPoints
            ?.whereType<DataPointLevel>()
            .toList() ??
        [];
    // data.removeWhere(
    //     (element) => element.levelT == null || (element.levelT is! num));
    return data;
  }

  @override
  Future<PaginatedData<Calibration>> getCalibrations(
      {required QueryParam params,
      String? deviceReference,
      CalibrationType? type,
      DateTime? start,
      DateTime? end,
      String? groupId}) {
    return _apiProvider.getCalibrations(
        params: params,
        deviceReference: deviceReference,
        type: type,
        start: start,
        end: end,
        groupId: groupId);
  }
}
