import 'dart:io';

import 'package:flutter_nanolike_sdk/data/api/requests/payload/device_payload.dart';
import 'package:flutter_nanolike_sdk/domain/models/accessory.dart';
import 'package:flutter_nanolike_sdk/domain/models/calibration.dart';
import 'package:flutter_nanolike_sdk/domain/models/device.dart';
import 'package:flutter_nanolike_sdk/domain/models/device_operation_type.dart';
import 'package:flutter_nanolike_sdk/domain/models/filters.dart';
import 'package:flutter_nanolike_sdk/domain/models/graph_data.dart';
import 'package:flutter_nanolike_sdk/domain/models/ibc_model.dart';
import 'package:flutter_nanolike_sdk/domain/models/media.dart';
import 'package:flutter_nanolike_sdk/domain/models/paginated_data.dart';
import 'package:flutter_nanolike_sdk/domain/models/group.dart';
import 'package:flutter_nanolike_sdk/domain/models/query_param.dart';
import 'package:flutter_nanolike_sdk/domain/models/report.dart';

abstract class IDevicesPresenter {
  Future<PaginatedData<Device>> dashboard(
      {required QueryParam queryParam, Filters? filters});

  Future<PaginatedData<Device>> fetchDevicesInstalledPaginated(
      QueryParam queryParam);

  Future<Device> fetchDevice({required String deviceId});

  Future<PaginatedData<IBCModel>> fetchIBCModels(QueryParam queryParam);

  Future<List<Device>> fetchGroupDevices(String groupId);

  Future<List<Device>> fetchDevicesWithGraphDataForGroups(List<Group> groups);

  Future<Report> validateDeviceIBC({required String deviceReference});

  Future<Device> createDevice({required DevicePayload devicePayload});

  Future<Device> updateDevice(
      {required String deviceReference, required DevicePayload devicePayload});

  Future<PaginatedData<Calibration>> getCalibrations(
      {required QueryParam params,
      String? deviceReference,
      CalibrationType? type,
      DateTime? start,
      DateTime? end,
      String? groupId});

  Future<void> deleteDevice({required String deviceReference});

  Future<Device> getDevice({required String deviceReference});

  Future<Accessory?> getAccessory(
      {required String accessoryReference, required String groupId});

  Future<void> createCalibrationLevel(
      {required String deviceId, required num level, DateTime? date});

  Future<void> createCalibrationDelivery(
      String deviceId, DateTime date, num quantity);

  Future<List<Media>> getDeviceMedias(String deviceId);

  Future<List<Media>> createDeviceMedia(String deviceId, File media);

  Future<void> deleteDeviceMedia(String mediaId);

  Future<void> maintainDevice(
      {required String deviceToMaintainReference,
      String? newDeviceReference,
      required DeviceOperationType deviceOperationType,
      String? comment});

  Future<List<DataPointValue>> dailyConsumption(
      {required String deviceReference,
      required DateTime from,
      required DateTime to});

  Future<List<DataPointLevel>> levelHistory(
      {required String deviceReference,
      required DateTime from,
      required DateTime to});
}
