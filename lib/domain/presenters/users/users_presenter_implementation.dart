import 'dart:async';
import 'dart:ui';

import 'package:flutter_nanolike_sdk/domain/models/group.dart';
import 'package:flutter_nanolike_sdk/domain/models/group_membership.dart';
import 'package:flutter_nanolike_sdk/domain/models/paginated_data.dart';
import 'package:flutter_nanolike_sdk/domain/models/query_param.dart';
import 'package:flutter_nanolike_sdk/domain/models/role.dart';
import 'package:flutter_nanolike_sdk/domain/models/user.dart';
import 'package:flutter_nanolike_sdk/domain/presenters/users/users_presenter.dart';
import 'package:flutter_nanolike_sdk/domain/providers/api_provider.dart';

class UsersPresenterImplementation implements IUsersPresenter {
  final IWebApiJWTProvider _apiProvider;

  UsersPresenterImplementation(this._apiProvider);

  @override
  Future<User> getUser(String id) async {
    return await _apiProvider.getUser(id);
  }

  @override
  Future<void> deleteUser(String id) async {
    return await _apiProvider.deleteAccount(id);
  }

  @override
  Future<PaginatedData<User>> getUsersPaginated(QueryParam params) async {
    return await _apiProvider.getUsersPaginated(params);
  }

  @override
  Future<void> addUserToGroup(
      {required User user, required Group group}) async {
    return await _apiProvider.addUserToGroup(user: user, group: group);
  }

  @override
  Future<void> deleteUserFromGroupMembership(
      {required User user, required GroupMembership groupMembership}) async {
    return await _apiProvider.deleteUserFromGroupMembership(
        user: user, groupMembership: groupMembership);
  }

  @override
  Future<User> updateUser(
      {required String userId,
      String? firstName,
      String? lastName,
      Locale? preferredLanguage,
      String? roleId}) async {
    return await _apiProvider.updateUser(
        userId: userId,
        firstName: firstName,
        lastName: lastName,
        preferredLanguage: preferredLanguage,
        roleId: roleId);
  }

  @override
  Future<List<Role>> getMeRoles() async {
    return await _apiProvider.getMeRoles();
  }

  @override
  Future<PaginatedData<Role>> getMeRolesPaginated(QueryParam params) async {
    return await _apiProvider.getMeRolesPaginated(params);
  }

  @override
  Future<PaginatedData<Locale>> getLanguages(QueryParam params) async {
    final languages = _apiProvider.getLanguages();
    final List<Locale> data = (params.page == 1) ? languages : [];
    return Future.value(PaginatedData(
        data: data, page: 1, pageSize: data.length, pageCount: 1));
  }

  @override
  Future<User> reinvite(String id) {
    return _apiProvider.reinvite(userId: id);
  }
}
