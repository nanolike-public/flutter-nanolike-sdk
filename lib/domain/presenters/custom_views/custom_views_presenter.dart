import 'package:flutter_nanolike_sdk/domain/models/custom_view.dart';
import 'package:flutter_nanolike_sdk/domain/models/filters.dart';
import 'package:flutter_nanolike_sdk/domain/models/paginated_data.dart';
import 'package:flutter_nanolike_sdk/domain/models/query_param.dart';

abstract class ICustomViewsPresenter {
  Future<PaginatedData<CustomView>> getCustomViewsPaginated(QueryParam params);

  Future<CustomView> getCustomView(String customViewId);

  Future<CustomView> createCustomView(String name, Filters filters);

  Future<CustomView> updateCustomView(
      String customViewId, String name, Filters filters);

  Future<void> deleteCustomView(String customViewId);
}
