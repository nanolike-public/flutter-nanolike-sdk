import 'package:flutter_nanolike_sdk/domain/models/accessory.dart';
import 'package:flutter_nanolike_sdk/domain/presenters/accessories/accessories_presenter.dart';
import 'package:flutter_nanolike_sdk/domain/providers/api_provider.dart';

class AccessoriesPresenterImplementation implements IAccessoriesPresenter {
  final IWebApiJWTProvider _apiProvider;

  AccessoriesPresenterImplementation(this._apiProvider);

  @override
  Future<List<Accessory>> fetchAccessories(String groupId) async {
    return await _apiProvider.fetchGroupAccessories(groupId);
  }

  @override
  Future<Accessory> createAccessory(
      {required String groupId,
      required String accessoryId,
      required AccessoryType accessoryType}) async {
    return await _apiProvider.createAccessory(
        groupId: groupId,
        accessoryId: accessoryId,
        accessoryType: accessoryType);
  }

  @override
  Future<void> deleteAccessory({required int accessoryId}) async {
    return await _apiProvider.deleteAccessory(accessoryId);
  }
}
