import 'package:flutter_nanolike_sdk/domain/models/device_content.dart';
import 'package:flutter_nanolike_sdk/domain/models/device_content_type.dart';
import 'package:flutter_nanolike_sdk/domain/models/drug.dart';
import 'package:flutter_nanolike_sdk/domain/models/order.dart';
import 'package:flutter_nanolike_sdk/domain/models/paginated_data.dart';
import 'package:flutter_nanolike_sdk/domain/models/query_param.dart';
import 'package:flutter_nanolike_sdk/domain/models/supplement.dart';

abstract class IOrdersPresenter {
  Future<List<Order>> getOrders();
  Future<Order> getOrder(String orderId);
  Future<void> updateOrder(Order order);
  Future<void> createOrder(Order order);
  Future<PaginatedData<DeviceContent>> getDeviceContentsPaginated(
      QueryParam params);
  Future<PaginatedData<DeviceContentType>> getDeviceContentTypesPaginated(
      QueryParam params);
  Future<PaginatedData<Drug>> getDrugsPaginated(QueryParam params);
  Future<PaginatedData<Supplement>> getSupplementsPaginated(QueryParam params);
  Future<void> createDeviceContent(String deviceContent);
}
