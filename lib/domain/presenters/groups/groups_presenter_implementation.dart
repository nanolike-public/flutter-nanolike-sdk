import 'dart:ui';

import 'package:dio/dio.dart';
import 'package:flutter_nanolike_sdk/domain/models/nanolike_exception.dart';
import 'package:flutter_nanolike_sdk/domain/models/paginated_data.dart';
import 'package:flutter_nanolike_sdk/domain/models/query_param.dart';
import 'package:flutter_nanolike_sdk/domain/models/role.dart';
import 'package:flutter_nanolike_sdk/domain/models/group.dart';
import 'package:flutter_nanolike_sdk/domain/models/user.dart';
import 'package:flutter_nanolike_sdk/domain/presenters/groups/groups_presenter.dart';
import 'package:flutter_nanolike_sdk/domain/providers/api_provider.dart';

class GroupsPresenterImplementation implements IGroupsPresenter {
  final IWebApiJWTProvider _apiProvider;

  GroupsPresenterImplementation(this._apiProvider);

  @override
  Future<PaginatedData<Group>> fetchGroupsPaginated(QueryParam params) async {
    return await _apiProvider.fetchGroupsPaginated(params, false);
  }

  @override
  Future<PaginatedData<Group>> fetchGroupsPoiPaginated(
      QueryParam params) async {
    return await _apiProvider.fetchGroupsPaginated(params, true);
  }

  @override
  Future<Group> fetchGroup(String groupId) async {
    return await _apiProvider.fetchGroup(groupId);
  }

  @override
  Future<Group> createGroup(
      final String name,
      final String? customId,
      final String? postalAddress,
      final String? zipCode,
      final String? town,
      final String? comment,
      final String? ownerEmail,
      final List<String> orderRecipients) async {
    try {
      return await _apiProvider.createGroup(name, customId, postalAddress,
          zipCode, town, comment, ownerEmail, orderRecipients);
    } catch (error) {
      if (error is DioException && error.response?.statusCode == 400) {
        throw NanolikeException(type: NanolikeExceptionType.groupAlreadyExist);
      } else if (error is DioException && error.response?.statusCode == 403) {
        throw NanolikeException(
            type: NanolikeExceptionType.noPermissionToCreateGroup);
      } else {
        throw NanolikeException(
            type: NanolikeExceptionType.createGroupCommonError);
      }
    }
  }

  @override
  Future<Group> updateGroup(
    final String groupeId,
    final String? name,
    final String? customId,
    final String? postalAddress,
    final String? zipCode,
    final String? town,
    final String? comment,
  ) async {
    try {
      return await _apiProvider.updateGroup(
          groupeId, name, customId, postalAddress, zipCode, town, comment);
    } catch (error) {
      if (error is DioException && error.response?.statusCode == 400) {
        throw NanolikeException(type: NanolikeExceptionType.groupAlreadyExist);
      } else if (error is DioException && error.response?.statusCode == 403) {
        throw NanolikeException(
            type: NanolikeExceptionType.noPermissionToCreateGroup);
      } else {
        throw NanolikeException(
            type: NanolikeExceptionType.createGroupCommonError);
      }
    }
  }

  @override
  Future<User> invite(
      {required String email,
      String? firstName,
      String? lastName,
      required List<String> groupIds,
      required Locale preferredLanguage,
      required Role role}) async {
    try {
      return await _apiProvider.invite(
          email: email,
          firstName: firstName,
          lastName: lastName,
          groupIds: groupIds,
          locale: preferredLanguage,
          role: role);
    } catch (errorInvite) {
      try {
        return await _apiProvider.reinvite(userId: email);
      } catch (error) {
        if (error is DioException && error.response?.statusCode == 400) {
          throw NanolikeException(
              type: NanolikeExceptionType.inviteAlreadyExistsError);
        }
        throw NanolikeException(type: NanolikeExceptionType.inviteError);
      }
    }
  }

  @override
  Future<void> deleteGroup(String groupId) async {
    try {
      return await _apiProvider.deleteGroup(groupId);
    } catch (error) {
      if (error is DioException && error.response?.statusCode == 403) {
        throw NanolikeException(
            type: NanolikeExceptionType.noPermissionToDeleteGroup);
      } else if (error is DioException && error.response?.statusCode == 409) {
        throw NanolikeException(
            type: NanolikeExceptionType.deleteGroupWithDevicesError);
      }
      throw NanolikeException(type: NanolikeExceptionType.deleteGroupError);
    }
  }
}
