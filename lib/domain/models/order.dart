import 'package:flutter_nanolike_sdk/domain/models/device.dart';
import 'package:flutter_nanolike_sdk/domain/models/group.dart';
import 'package:flutter_nanolike_sdk/helpers/extensions.dart';

class Order {
  final String? id;
  final DateTime? date;
  final DateTime? dateRangeEnd;
  final bool? bypassSendEmail;
  final TimeSlot? timeSlot;
  final num? totalQuantity;
  final OrderSource? source;
  final List<OrderDevice> orderDevices;
  final String? round;
  final String? comment;

  Order(
      {this.id,
      this.date,
      this.dateRangeEnd,
      this.bypassSendEmail,
      this.timeSlot,
      this.totalQuantity,
      this.source,
      this.orderDevices = const [],
      this.round,
      this.comment});

  factory Order.fromJson(Map<String, dynamic> json) {
    final List<OrderDevice> silos = [];
    final List<dynamic>? silosJson = json['silos'];
    if (silosJson != null && silosJson.isNotEmpty) {
      silos.addAll(
          silosJson.map<OrderDevice>((e) => OrderDevice.fromJson(e)).toList());
    }

    return Order(
        id: json['id'],
        date:
            DateTime.tryParse(json['date']?.toString() ?? "") ?? DateTime.now(),
        dateRangeEnd:
            DateTime.tryParse(json['date_range_end']?.toString() ?? ""),
        totalQuantity: json['totalTonnage'],
        comment: json['comment'],
        round: json['round'],
        source: OrderSource.values.firstWhereOrNull(
            (element) => element.requestValue == json['source']),
        timeSlot: TimeSlot.values.firstWhereOrNull(
            (element) => element.requestValue == json['timeSlot']),
        orderDevices: silos);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (date != null) {
      data['date'] = date?.toIso8601String();
    }
    if (dateRangeEnd != null) {
      data['date_range_end'] = dateRangeEnd?.toIso8601String();
    }
    if (timeSlot != null) {
      data['timeSlot'] = timeSlot?.requestValue;
    }
    if (bypassSendEmail != null) {
      data['bypass_send_email'] = bypassSendEmail;
    }
    if (round != null) {
      data['round'] = round;
    }
    if (comment != null) {
      data['comment'] = comment;
    }

    if (source != null) {
      data['source'] = source?.requestValue;
    }

    data['silos'] = orderDevices.map((e) => e.toJson()).toList();
    return data;
  }

  @override
  bool operator ==(Object other) =>
      other is Order && other.runtimeType == runtimeType && other.id == id;

  @override
  int get hashCode => id.hashCode;

  List<Group> get groupsPoi => orderDevices
      .where((element) => element.groupPoi != null)
      .map((e) => e.groupPoi!)
      .toList();
}

class OrderDevice implements Comparable {
  final String deviceReference;
  final String? deviceName;
  final num quantity;
  final String? siloNumber;
  final String? comment;
  final num? deviceCapacity;
  final List<String>? groupIds;
  final Group? groupPoi;
  final OrderDeviceContent? content;

  OrderDevice(
      {required this.deviceReference,
      this.deviceName,
      required this.quantity,
      this.siloNumber,
      this.comment,
      this.deviceCapacity,
      this.groupIds,
      this.groupPoi,
      this.content});

  factory OrderDevice.fromJson(Map<String, dynamic> json) {
    Group? groupPoi;
    if (json['group_poi'] != null) {
      final groupPoiJson = json['group_poi'];
      if (groupPoiJson["group_id"] != null &&
          groupPoiJson["group_name"] != null) {
        groupPoi = Group(
            id: groupPoiJson["group_id"],
            name: groupPoiJson["group_name"],
            devices: [
              Device(
                  id: json['id'],
                  reference: json['id'],
                  name: json['device_name'],
                  groupPoi: Group(
                      id: groupPoiJson["group_id"],
                      name: groupPoiJson["group_name"]))
            ]);
      }
    }
    return OrderDevice(
        deviceReference: json['id'],
        quantity: json['tonnage'],
        deviceName: json['device_name'],
        comment: json['comment'],
        siloNumber: json['silo_number']?.toString(),
        groupPoi: groupPoi,
        deviceCapacity: null,
        groupIds:
            json['group_ids'] != null ? List.from(json['group_ids']) : null,
        content: json['content'] != null
            ? OrderDeviceContent.fromJson(json['content'])
            : null);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = deviceReference;
    data['tonnage'] = quantity;
    if (comment != null) {
      data['comment'] = comment;
    }
    if (deviceName != null) {
      data['device_name'] = deviceName;
    }
    if (siloNumber != null) {
      data['silo_number'] = siloNumber;
    }
    if (groupPoi != null) {
      data['farm_name'] = groupPoi!.name;
    }
    if (content != null) {
      data['content'] = content?.toJson();
    }

    return data;
  }

  @override
  bool operator ==(Object other) =>
      other is OrderDevice &&
      other.runtimeType == runtimeType &&
      other.deviceReference == deviceReference;

  @override
  int get hashCode => deviceReference.hashCode;

  @override
  int compareTo(other) {
    final String? groupPoiNameUnWrap = groupPoi?.name;
    final String? otherGroupPoiNameUnWrap = other.groupPoi?.name;
    final String? deviceNameUnWrap = deviceName;
    final String? otherDeviceNameUnWrap = other.deviceName;

    if (groupPoiNameUnWrap != null && otherGroupPoiNameUnWrap != null) {
      int groupComp = groupPoiNameUnWrap.compareTo(otherGroupPoiNameUnWrap);
      if (groupComp == 0 &&
          (deviceNameUnWrap != null && otherDeviceNameUnWrap != null)) {
        return deviceNameUnWrap.compareTo(otherDeviceNameUnWrap);
      }
      return groupComp;
    } else if (deviceNameUnWrap != null && otherDeviceNameUnWrap != null) {
      return deviceNameUnWrap.compareTo(otherDeviceNameUnWrap);
    } else {
      return 0;
    }
  }
}

class OrderDeviceContent {
  final String? name;
  final String? type;
  final String? drug;
  final OrderDeviceContentSupplement? supplement;

  OrderDeviceContent({this.name, this.type, this.drug, this.supplement});

  OrderDeviceContent.fromJson(Map<String, dynamic> json)
      : name = json['name'],
        type = json['type'],
        drug = json['drug'],
        supplement = OrderDeviceContentSupplement.fromJson(json['supplement']);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (name != null) {
      data['name'] = name;
    }
    if (type != null) {
      data['type'] = type;
    }
    if (drug != null) {
      data['drug'] = drug;
    }
    if (supplement != null) {
      data['supplement'] = supplement?.toJson();
    }
    return data;
  }
}

class OrderDeviceContentSupplement {
  final String? name;
  final int? ratio;

  OrderDeviceContentSupplement({this.name, this.ratio});

  OrderDeviceContentSupplement.fromJson(Map<String, dynamic> json)
      : name = json['name'],
        ratio = json['ratio'];

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (name != null) {
      data['name'] = name;
    }
    if (ratio != null) {
      data['ratio'] = ratio;
    }
    return data;
  }
}

enum TimeSlot { morning, afternoon }

enum OrderSource { delivery, order }

extension OrderTimeSlotExtension on TimeSlot {
  String get requestValue {
    switch (this) {
      case TimeSlot.morning:
        return "morning";
      case TimeSlot.afternoon:
        return "afternoon";
    }
  }
}

extension OrderSourceExtension on OrderSource {
  String get requestValue {
    switch (this) {
      case OrderSource.delivery:
        return "delivery";
      case OrderSource.order:
        return "order";
    }
  }
}
