class Supplement {
  final String id;
  final String label;

  Supplement({required this.id, required this.label});

  factory Supplement.fromJson(Map<String, dynamic> json) {
    return Supplement(
      id: json['supplement'],
      label: json['supplement'],
    );
  }
}
