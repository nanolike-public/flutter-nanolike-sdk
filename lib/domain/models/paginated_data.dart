class PaginatedData<T> {
  final List<T> data;
  final num page;
  final num pageSize;
  final num pageCount;
  final num? rowCount;

  PaginatedData(
      {required this.data,
      required this.page,
      required this.pageSize,
      required this.pageCount,
      this.rowCount});

  factory PaginatedData.fromJson(
      {required Map<String, dynamic> json, required List<T> items}) {
    return PaginatedData<T>(
        data: items,
        page: json['page'] ?? 0,
        pageSize: json['pageSize'] ?? 20,
        pageCount: json['pageCount'] ?? 0,
        rowCount: json['rowCount']);
  }
}
