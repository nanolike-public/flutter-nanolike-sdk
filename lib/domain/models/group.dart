import 'package:flutter_nanolike_sdk/domain/models/accessory.dart';
import 'package:flutter_nanolike_sdk/domain/models/device.dart';
import 'package:flutter_nanolike_sdk/domain/models/role.dart';

class Group implements Comparable {
  final String id;
  final String name;
  final String? address;
  final String? city;
  final String? zipCode;
  final bool isPoi;
  final String? customId;
  final Role? userRole;
  final List<String> orderRecipientEmails;
  final List<Device> devices;
  final List<Accessory> accessories;
  final String? comment;

  Group(
      {required this.id,
      required this.name,
      this.address,
      this.city,
      this.zipCode,
      this.isPoi = false,
      this.customId,
      this.userRole,
      this.comment,
      this.orderRecipientEmails = const [],
      this.devices = const [],
      this.accessories = const []});

  factory Group.fromAPIJson(Map<String, dynamic> json) {
    return Group(
      id: json['group_id'],
      name: json['group_name'],
      address: json['address_street'],
      city: json['address_town'],
      zipCode: json['address_zipcode'],
      comment: json['comment'],
      isPoi: json['is_poi'] ?? false,
      customId: json['client_poi_id'],
      userRole: json['user_role_in_group'] != null
          ? Role.fromGroupJson(json['user_role_in_group'])
          : null,
      orderRecipientEmails: json['order_recipient_emails'] != null
          ? List.from(json['order_recipient_emails'])
          : [],
      devices: json['devices'] != null
          ? json['devices']
              .map<Device>((e) => Device.fromDevicesJson(e))
              .toList()
          : [],
      accessories: json['accessories'] != null
          ? json['accessories']
              .map<Accessory>((e) => Accessory.fromJson(e))
              .toList()
          : [],
    );
  }

  factory Group.fromJson(Map<String, dynamic> json) {
    return Group(id: json['id'], name: json['name']);
  }

  Map<String, dynamic> toJson() {
    return {'id': id, 'name': name};
  }

  List<Device> get devicesCombined =>
      devices.where((element) => element.isCombined == true).toList();
  List<Device> get devicesNotCombined =>
      devices.where((element) => element.isCombined == false).toList();

  List<Device> get devicesInProblem =>
      devices.where((element) => element.isProblem == true).toList();

  @override
  bool operator ==(Object other) =>
      other is Group &&
      other.runtimeType == runtimeType &&
      (other.id == id || other.name == name);

  @override
  int get hashCode => id.hashCode;

  @override
  int compareTo(other) {
    return name.compareTo(other.name);
  }
}
