class Session {
  final String? workspace;

  Session(this.workspace);

  factory Session.fromJson(Map<String, dynamic> json) {
    return Session(json['workspace']);
  }

  Map<String, dynamic> toJson() => {'workspace': workspace};
}
