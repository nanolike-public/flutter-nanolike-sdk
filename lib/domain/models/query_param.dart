class CustomQueryParam {
  final String key;
  final String value;

  CustomQueryParam({required this.key, required this.value});
}

class QueryParam {
  final num page;
  final num pageSize;
  final String? search;
  final String? ordering;
  final List<CustomQueryParam>? customQueryParams;

  const QueryParam(
      {this.page = 1,
      this.pageSize = 20,
      this.search,
      this.ordering,
      this.customQueryParams});

  Map<String, dynamic> toMap() {
    return {
      'page': page,
      'page_size': pageSize,
      if (search != null && search!.isNotEmpty) 'search': search,
      if (ordering != null && ordering!.isNotEmpty) 'ordering': ordering,
      if (customQueryParams != null && customQueryParams!.isNotEmpty)
        for (final customQueryParam in customQueryParams!)
          customQueryParam.key: customQueryParam.value
    };
  }
}
