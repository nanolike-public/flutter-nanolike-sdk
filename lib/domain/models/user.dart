import 'dart:ui';

import 'package:flutter_nanolike_sdk/domain/models/group.dart';
import 'package:flutter_nanolike_sdk/domain/models/group_membership.dart';
import 'package:flutter_nanolike_sdk/domain/models/locale_extension.dart';
import 'package:flutter_nanolike_sdk/domain/models/menu.dart';
import 'package:flutter_nanolike_sdk/domain/models/role.dart';
import 'package:flutter_nanolike_sdk/domain/models/workspace.dart';
import 'package:flutter_nanolike_sdk/helpers/authorization_helper.dart';

class User {
  final String id;
  final String username;
  final String? email;
  final String? firstName;
  final String? lastName;
  final Role? workspaceRole;
  final DateTime? lastLogin;
  final DateTime? inviteExpireAt;
  final List<Workspace>? workspaces;
  final bool? isInvitationPending;
  final UserConfiguration? configuration;
  final List<GroupMembership>? groupMemberships;
  final List<NanolikeMenu> menu;
  User(
      {required this.id,
      required this.username,
      this.email,
      this.firstName,
      this.lastName,
      this.workspaceRole,
      this.workspaces,
      this.lastLogin,
      this.inviteExpireAt,
      this.isInvitationPending,
      this.configuration,
      this.groupMemberships,
      this.menu = const <NanolikeMenu>[]});

  String get fullName {
    if (firstName != null && lastName != null) {
      return '$firstName $lastName';
    } else if (firstName != null) {
      return firstName!;
    } else if (lastName != null) {
      return lastName!;
    } else {
      return username;
    }
  }

  List<Group> get groups {
    final List<Group> g = groupMemberships?.map((e) => e.group).toList() ?? [];
    g.sort();
    return g;
  }
  //groupMemberships?.map((e) => e.group).toList() ?? [];

  factory User.fromJson(Map<String, dynamic> json) {
    // parse configuration
    UserConfiguration? configuration;
    final Map<String, dynamic>? conf = json['configuration'];
    if (conf != null) {
      configuration = UserConfiguration.fromJson(conf);
    }

    // parse workspace role
    Role? workspaceRole;
    final Map<String, dynamic>? wr = json['workspace_role'];
    if (wr != null) {
      workspaceRole = Role.fromJson(wr);
    }

    // parse is_invitation_pending
    final num? isInvitationPending = json['is_invitation_pending'];

    // parse groups
    List<GroupMembership> groupMemberships = [];
    List<dynamic>? gm = json['group_memberships'];
    if (gm != null && gm.isNotEmpty) {
      groupMemberships =
          gm.map<GroupMembership>((e) => GroupMembership.fromJson(e)).toList();
    }

    return User(
        id: json['idUser'],
        username: json['username'],
        email: json['email'],
        firstName: json['first_name'],
        lastName: json['last_name'],
        configuration: configuration,
        workspaceRole: workspaceRole,
        groupMemberships: groupMemberships,
        lastLogin: json['last_login'] != null
            ? DateTime.tryParse(json['last_login'])
            : null,
        inviteExpireAt: json['invite_expire_at'] != null
            ? DateTime.tryParse(json['invite_expire_at'])
            : null,
        isInvitationPending:
            (isInvitationPending != null && isInvitationPending == 1)
                ? true
                : false);
  }

  factory User.fromMeJson(Map<String, dynamic> json) {
    // parse workspaces
    final List<Workspace> workspaces = [];
    final List<dynamic>? clients = json['clients'];
    if (clients != null && clients.isNotEmpty) {
      workspaces.addAll(clients.map((e) => Workspace.fromMeJson(e)));
    }

    workspaces.sort();

    // parse configuration
    UserConfiguration? configuration;
    final Map<String, dynamic>? conf = json['configuration'];
    if (conf != null) {
      configuration = UserConfiguration.fromJson(conf);
    }

    // parse current role
    Role? workspaceRole;
    List<NanolikeMenu> menu = [];
    final Map<String, dynamic>? workspaceRoleJson = json['workspace_role'];
    if (workspaceRoleJson != null) {
      workspaceRole = Role.fromJson(workspaceRoleJson);
      final authorizationServer = workspaceRole.authorization;
      // parse authorizations to determine menus can be displayed
      if (AuthorizationHelper()
          .hasAuthorization(authorizationServer, 'menu_devices', ['READ'])) {
        menu.add(NanolikeMenu.devices);
      }
      if (AuthorizationHelper()
          .hasAuthorization(authorizationServer, 'menu_orders', ['READ'])) {
        menu.add(NanolikeMenu.orders);
      }
      if (AuthorizationHelper()
          .hasAuthorization(authorizationServer, 'menu_users', ['READ'])) {
        menu.add(NanolikeMenu.users);
      }
      if (AuthorizationHelper().hasAuthorization(
          authorizationServer, 'menu_setup_silos', ['READ'])) {
        if (menu.contains(NanolikeMenu.devices)) {
          menu.add(NanolikeMenu.setupSilos);
        } else {
          menu.insert(0, NanolikeMenu.setupSilos);
        }
      }
      if (AuthorizationHelper()
          .hasAuthorization(authorizationServer, 'menu_setup_ibc', ['READ'])) {
        if (menu.contains(NanolikeMenu.devices)) {
          menu.add(NanolikeMenu.setupIBC);
        } else {
          menu.insert(0, NanolikeMenu.setupIBC);
        }
      }
    }

    menu.add(NanolikeMenu.settings);

    // parse is_invitation_pending
    final num? isInvitationPending = json['is_invitation_pending'];

    return User(
        id: json['idUser'],
        username: json['username'],
        email: json['email'],
        configuration: configuration,
        firstName: json['first_name'],
        lastName: json['last_name'],
        workspaces: workspaces,
        lastLogin: json['last_login'] != null
            ? DateTime.tryParse(json['last_login'])
            : null,
        isInvitationPending:
            (isInvitationPending != null && isInvitationPending == 1)
                ? true
                : false,
        inviteExpireAt: json['invite_expire_at'] != null
            ? DateTime.tryParse(json['invite_expire_at'])
            : null,
        workspaceRole: workspaceRole,
        menu: menu);
  }
}

class UserConfiguration {
  final String? preferredLanguageString;
  final Locale? preferredLanguage;
  final String? preferredTimezone;
  final bool? notificationPush;
  final bool? notificationEmail;
  final bool? displayForecast;
  final bool? displayRemaining;
  final bool? displayLastOrder;
  final bool? displayQuickActions;
  final bool? displayQuickActionOrder;

  UserConfiguration(
      {this.preferredLanguageString,
      this.preferredLanguage,
      this.preferredTimezone,
      this.notificationPush,
      this.notificationEmail,
      this.displayForecast,
      this.displayRemaining,
      this.displayLastOrder,
      this.displayQuickActions,
      this.displayQuickActionOrder});

  factory UserConfiguration.fromJson(Map<String, dynamic> json) {
    // parse preferred_language
    final String? preferredLanguageString = json['preferred_language'];
    Locale? preferredLanguage;
    if (preferredLanguageString != null && preferredLanguageString.isNotEmpty) {
      preferredLanguage = LocaleUtils.fromString(preferredLanguageString);
    }

    return UserConfiguration(
        preferredLanguageString: json['preferred_language'],
        preferredLanguage: preferredLanguage,
        preferredTimezone: json['preferred_timezone'],
        notificationPush: json['notification_push'],
        notificationEmail: json['notification_email'],
        displayForecast: json['display_forecast'],
        displayRemaining: json['display_remaining'],
        displayLastOrder: json['display_last_order'],
        displayQuickActions: json['display_quick_actions'],
        displayQuickActionOrder: json['display_quick_action_order']);
  }
}
