enum NanolikeExceptionType {
  accountDeactivated,
  workspaceMismatch,
  badCredentials,
  calibrationAlreadyExistForSiloDate,
  createDeviceCommonError,
  createGroupCommonError,
  dashboardForbidden,
  deleteDeviceCommonError,
  deleteGroupError,
  deleteGroupWithDevicesError,
  deviceAlreadyExist,
  deviceNotFound,
  deviceNotProvisioned,
  fileTooLarge,
  forbidden,
  forgotPasswordEmailNotFound,
  forgotPasswordWorkspaceNotFound,
  inviteAlreadyExistsError,
  inviteError,
  noClientExisting,
  noPermissionToCreateDevice,
  noPermissionToCreateGroup,
  noPermissionToDeleteDevice,
  noPermissionToDeleteGroup,
  noPermissionToForgotPassword,
  orderAlreadyExistForSiloDate,
  deliveryAlreadyExistForSiloDate,
  deliveryQuantityNotValid,
  levelQuantityNotValid,
  orderNotFound,
  other,
  groupAlreadyExist,
  tooManyMedia,
  unauthorizedWorkspace,
  wrongNewPassword,
  wrongOldPassword
}

class NanolikeException implements Exception {
  NanolikeExceptionType type;

  NanolikeException({
    this.type = NanolikeExceptionType.other,
  });
}
