class DeviceContentType {
  final String id;
  final String name;
  final num? fixedQuantityTons;
  final num? minQuantityTons;

  DeviceContentType(
      {required this.id,
      required this.name,
      this.fixedQuantityTons,
      this.minQuantityTons});

  factory DeviceContentType.fromJson(Map<String, dynamic> json) {
    num? fixedQuantityTons = json['fixed_quantity_tons'];
    num? minQuantityTons = json['min_quantity_tons'];

    return DeviceContentType(
        id: json['id'],
        name: json['device_content_type'],
        fixedQuantityTons:
            (fixedQuantityTons == minQuantityTons) ? null : fixedQuantityTons,
        minQuantityTons: json['min_quantity_tons']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['device_content_type'] = name;
    if (fixedQuantityTons != null) {
      data['fixed_quantity_tons'] = fixedQuantityTons;
    }
    if (minQuantityTons != null) {
      data['min_quantity_tons'] = minQuantityTons;
    }
    return data;
  }

  @override
  bool operator ==(Object other) =>
      other is DeviceContentType &&
      other.runtimeType == runtimeType &&
      (other.id == id || other.name == name);

  @override
  int get hashCode => id.hashCode;
}
