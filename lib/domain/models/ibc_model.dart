class IBCModel {
  final String name;
  IBCModel({required this.name});

  factory IBCModel.fromJson(Map<String, dynamic> json) {
    return IBCModel(name: json['ibc_model']);
  }
}
