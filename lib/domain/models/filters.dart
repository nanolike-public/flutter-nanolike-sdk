import 'package:flutter_nanolike_sdk/domain/models/device_content.dart';
import 'package:flutter_nanolike_sdk/domain/models/group.dart';

class Filters {
  final List<Group>? groups;
  final List<DeviceContent>? deviceContents;
  final List<int>? levels;
  final List<int>? remainingDays;
  final List<String>? statuses;
  final num? levelTonsLessThan;
  final num? levelTonsGreaterThan;
  final num? missingWeightLessThan;
  final num? missingWeightGreaterThan;

  Filters(
      {this.groups,
      this.deviceContents,
      this.levels,
      this.remainingDays,
      this.statuses,
      this.levelTonsLessThan,
      this.levelTonsGreaterThan,
      this.missingWeightLessThan,
      this.missingWeightGreaterThan});

  factory Filters.fromJson(Map<String, dynamic> json) {
    return Filters(
      groups: json['groups']?.map<Group>((e) => Group.fromJson(e)).toList(),
      deviceContents: json['deviceContents']
          ?.map<DeviceContent>((e) => DeviceContent.fromJson(e))
          .toList(),
      statuses:
          json['status'] != null ? List<String>.from(json['status']) : null,
      levels: json['levels'] != null ? List<int>.from(json['levels']) : null,
      remainingDays: json['remainingDays'] != null
          ? List<int>.from(json['remainingDays'])
          : null,
      levelTonsLessThan: json['levelTonsLessThan'],
      levelTonsGreaterThan: json['levelTonsGreaterThan'],
      missingWeightLessThan: json['missingWeightLessThan'],
      missingWeightGreaterThan: json['missingWeightGreaterThan'],
    );
  }

  factory Filters.fromJsonFromCustomView(Map<String, dynamic> json) {
    List<Group>? groups;
    List<DeviceContent>? deviceContents;
    List<int>? levels;
    List<int>? remainingDays;
    List<String>? statuses;

    if (json['groups'] != null) {
      groups = json['groups']?.map<Group>((e) => Group.fromAPIJson(e)).toList();
    } else if (json['groupIds'] != null) {
      groups =
          json['groupIds']?.map<Group>((e) => Group(id: e, name: e)).toList();
    }

    if (json['deviceContents'] != null) {
      deviceContents = json['deviceContents']
          ?.map<DeviceContent>((e) => DeviceContent.fromAPIJson(e))
          .toList();
    } else if (json['deviceContentIds'] != null) {
      deviceContents = json['deviceContentIds']
          ?.map<DeviceContent>((e) => DeviceContent(id: e, name: e))
          .toList();
    }

    if (json['levels'] != null) {
      final List<String> levelsString = List<String>.from(json['levels']);
      levels = levelsString.map((level) => int.parse(level)).toList();
    }

    if (json['remainingDays'] != null) {
      final List<String> remainingDaysString =
          List<String>.from(json['remainingDays']);
      remainingDays = remainingDaysString.map((e) => int.parse(e)).toList();
    }

    if (json['status'] != null) {
      statuses = List<String>.from(json['status']);
    }

    return Filters(
      deviceContents: deviceContents,
      groups: groups,
      statuses: statuses,
      levels: levels,
      remainingDays: remainingDays,
    );
  }

  Map<String, dynamic> _removeNulls(Map<String, dynamic> json) {
    final filteredJson = Map<String, dynamic>.from(json);
    filteredJson.removeWhere((key, value) => value == null);
    return filteredJson;
  }

  Map<String, dynamic> toJsonFromCustomView() {
    final json = {
      'groupIds': groups?.map((e) => e.id).toList(),
      'deviceContentIds': deviceContents?.map((e) => e.id).toList(),
      'levels': levels?.map((e) => e.toString()).toList(),
      'status': statuses,
      'remainingDays': remainingDays?.map((e) => e.toString()).toList(),
    };
    return _removeNulls(json);
  }

  Map<String, dynamic> toJson() {
    final json = {
      'groups': groups?.map((e) => e.toJson()).toList(),
      'deviceContents': deviceContents?.map((e) => e.toJson()).toList(),
      'levels': levels,
      'status': statuses,
      'remainingDays': remainingDays,
      'levelTonsLessThan': levelTonsLessThan,
      'levelTonsGreaterThan': levelTonsGreaterThan,
      'missingWeightLessThan': missingWeightLessThan,
      'missingWeightGreaterThan': missingWeightGreaterThan,
    };
    return _removeNulls(json);
  }

  int get nbFilters {
    return (groups?.length ?? 0) +
        (deviceContents?.length ?? 0) +
        (levels?.length ?? 0) +
        (statuses?.length ?? 0) +
        ((remainingDays != null && remainingDays!.isNotEmpty) ? 1 : 0);
  }
}

class FiltersUtils {
  Filters updateFilters<T>({
    required Filters? filters,
    required List<T>? items,
    required T item,
  }) {
    final List<T>? updatedItems;
    if (items != null && items.contains(item)) {
      final objectIndex = items.indexWhere((element) => element == item);
      updatedItems = items..removeAt(objectIndex);
    } else {
      updatedItems = [...items ?? [], item];
    }

    return Filters(
      deviceContents: T == DeviceContent
          ? updatedItems as List<DeviceContent>?
          : filters?.deviceContents,
      groups: T == Group ? updatedItems as List<Group>? : filters?.groups,
      remainingDays: filters?.remainingDays,
      levels: T == int ? updatedItems as List<int>? : filters?.levels,
      statuses: T == String ? updatedItems as List<String>? : filters?.statuses,
    );
  }
}
