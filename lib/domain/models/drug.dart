class Drug {
  final String id;
  final String label;

  Drug({required this.id, required this.label});

  factory Drug.fromJson(Map<String, dynamic> json) {
    return Drug(
      id: json['drug'],
      label: json['drug'],
    );
  }
}
