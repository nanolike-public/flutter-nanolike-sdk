import 'package:flutter_nanolike_sdk/domain/models/workspace_settings.dart';

class Workspace implements Comparable {
  final String name;
  final UseCase? useCase;

  Workspace({required this.name, this.useCase});

  factory Workspace.fromUserJson(Map<String, dynamic> json) {
    final String name = json['name'] as String;
    return Workspace(name: name.toLowerCase());
  }

  factory Workspace.fromMeJson(Map<String, dynamic> json) {
    final String name = json['name'] as String;

    // Parse use case
    UseCase? useCase;
    final String? useCaseSring = json['useCase'];
    if (useCaseSring != null && useCaseSring.isNotEmpty) {
      if (useCaseSring == 'silo') {
        useCase = UseCase.bin;
      } else if (useCaseSring == 'ibc') {
        useCase = UseCase.tank;
      } else if (useCaseSring == 'silo_industry') {
        useCase = UseCase.silo;
      }
    }

    return Workspace(name: name.toLowerCase(), useCase: useCase);
  }

  @override
  bool operator ==(Object other) =>
      other is Workspace &&
      other.runtimeType == runtimeType &&
      other.name == name;

  @override
  int get hashCode => name.hashCode;

  @override
  int compareTo(other) {
    return name.compareTo(other.name);
  }
}
