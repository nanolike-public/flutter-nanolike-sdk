import 'package:dio/dio.dart';
import 'package:flutter/services.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/get_device_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';

class ApiStubAdapter implements HttpClientAdapter {
  final HttpClientAdapter _adapter = HttpClientAdapter();
  final String packagePrefixPath = "packages/flutter_nanolike_sdk/";

  @override
  Future<ResponseBody> fetch(
    RequestOptions options,
    Stream<Uint8List>? requestStream,
    Future? cancelFuture,
  ) async {
    final request = options.extra['request'];

    // Force 404 to display Connectivity page during device creation process.
    if (request is GetDeviceRequest) {
      return Future.delayed(const Duration(milliseconds: 500), () {
        return ResponseBody.fromString("", 404);
      });
    }

    if (request is Request && request.mock.isNotEmpty) {
      final String jsonString =
          await rootBundle.loadString('$packagePrefixPath${request.mock}');
      final headers = {
        "content-type": ["application/json"]
      };
      return Future.delayed(const Duration(milliseconds: 500), () {
        return ResponseBody.fromString(jsonString, 200, headers: headers);
      });
    } else {
      return _adapter.fetch(options, requestStream, cancelFuture);
    }
  }

  @override
  void close({bool force = false}) {
    _adapter.close(force: force);
  }
}
