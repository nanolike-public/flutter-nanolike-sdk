import 'package:flutter_nanolike_sdk/domain/models/group.dart';

class GroupResponse {
  final Group group;

  GroupResponse(this.group);

  factory GroupResponse.parse(Map<String, dynamic> json) {
    return GroupResponse(Group.fromAPIJson(json));
  }
}
