import 'package:flutter_nanolike_sdk/domain/models/media.dart';

class GetDeviceMediaResponse {
  final Media media;

  GetDeviceMediaResponse(this.media);

  factory GetDeviceMediaResponse.parse(Map<String, dynamic> json) {
    return GetDeviceMediaResponse(Media.fromJson(json));
  }
}