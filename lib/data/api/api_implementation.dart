import 'dart:async';
import 'dart:developer';
import 'dart:io';
import 'dart:ui';

import 'package:dio/dio.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/create_accessory_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/create_calibration_level_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/create_custom_view_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/create_device_content_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/create_device_media_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/create_device_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/create_firebase_token_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/create_order_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/create_register_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/create_group_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/create_user_to_group_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/delete_accessory_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/delete_account_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/delete_custom_view_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/delete_device_media_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/delete_device_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/delete_firebase_token_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/delete_group_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/delete_user_from_group_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/get_calibrations_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/get_connectivity_messages_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/get_custom_view_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/get_custom_views_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/get_dashboard_device_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/get_dashboard_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/get_device_contents_paginated_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/get_device_contents_types_paginated_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/get_device_medias_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/get_device_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/get_devices_paginated_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/get_drugs_paginated_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/get_front_config_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/get_graph_data_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/get_group_roles_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/get_groups_paginated_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/get_ibc_models_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/get_me_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/get_order_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/get_orders_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/get_register_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/get_roles_paginated_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/get_roles_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/get_group_accessories_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/get_group_devices_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/get_group_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/get_supplements_paginated_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/get_user_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/get_users_paginated_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/get_workspace_roles.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/get_workspace_settings_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/get_workspaces_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/login_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/invite_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/payload/device_payload.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/post_maintenance_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/post_validate_device_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/refresh_token_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/reinstall_device_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/reinvite_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/reset_password_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/update_custom_view_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/update_device_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/update_group_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/update_order_request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/update_user_request.dart';
import 'package:flutter_nanolike_sdk/data/api/response_models/get_connectivity_response.dart';
import 'package:flutter_nanolike_sdk/data/api/response_models/get_device_medias_response.dart';
import 'package:flutter_nanolike_sdk/data/api/response_models/get_graph_data_response.dart';
import 'package:flutter_nanolike_sdk/data/api/response_models/get_group_roles_response.dart';
import 'package:flutter_nanolike_sdk/data/api/response_models/get_orders_response.dart';
import 'package:flutter_nanolike_sdk/data/api/response_models/get_group_accessories_response.dart';
import 'package:flutter_nanolike_sdk/data/api/response_models/get_devices_response.dart';
import 'package:flutter_nanolike_sdk/data/api/response_models/get_group_response.dart';
import 'package:flutter_nanolike_sdk/data/api/response_models/get_workspace_roles_response.dart';
import 'package:flutter_nanolike_sdk/data/api/response_models/token_response.dart';
import 'package:flutter_nanolike_sdk/data/api/stub/api_stub_adapter.dart';
import 'package:flutter_nanolike_sdk/domain/models/accessory.dart';
import 'package:flutter_nanolike_sdk/domain/models/calibration.dart';
import 'package:flutter_nanolike_sdk/domain/models/connectivity_message.dart';
import 'package:flutter_nanolike_sdk/domain/models/custom_view.dart';
import 'package:flutter_nanolike_sdk/domain/models/device.dart';
import 'package:flutter_nanolike_sdk/domain/models/device_content.dart';
import 'package:flutter_nanolike_sdk/domain/models/device_content_type.dart';
import 'package:flutter_nanolike_sdk/domain/models/device_operation_type.dart';
import 'package:flutter_nanolike_sdk/domain/models/drug.dart';
import 'package:flutter_nanolike_sdk/domain/models/filters.dart';
import 'package:flutter_nanolike_sdk/domain/models/front_config.dart';
import 'package:flutter_nanolike_sdk/domain/models/graph_data.dart';
import 'package:flutter_nanolike_sdk/domain/models/group_membership.dart';
import 'package:flutter_nanolike_sdk/domain/models/ibc_model.dart';
import 'package:flutter_nanolike_sdk/domain/models/media.dart';
import 'package:flutter_nanolike_sdk/domain/models/order.dart';
import 'package:flutter_nanolike_sdk/domain/models/paginated_data.dart';
import 'package:flutter_nanolike_sdk/domain/models/group.dart';
import 'package:flutter_nanolike_sdk/domain/models/query_param.dart';
import 'package:flutter_nanolike_sdk/domain/models/report.dart';
import 'package:flutter_nanolike_sdk/domain/models/supplement.dart';
import 'package:flutter_nanolike_sdk/domain/models/token.dart';
import 'package:flutter_nanolike_sdk/domain/models/user.dart';
import 'package:flutter_nanolike_sdk/domain/models/role.dart';
import 'package:flutter_nanolike_sdk/domain/models/workspace.dart';
import 'package:flutter_nanolike_sdk/domain/models/workspace_settings.dart';
import 'package:flutter_nanolike_sdk/domain/providers/api_provider.dart';
import 'package:flutter/foundation.dart' as foundation;

class WebApiProviderImplementation implements IWebApiJWTProvider {
  late final BaseOptions _options;
  late final Dio _dio;
  late final Dio _dioRepeat;

  final StreamController<JWTToken?> _jwtTokenStreamController =
      StreamController<JWTToken?>();

  JWTToken? _token;
  String? _workspace;
  final String baseUrl;
  final bool isStub;
  final String productName;

  WebApiProviderImplementation(
      {required this.baseUrl,
      this.isStub = false,
      JWTToken? token,
      String? workspace,
      required this.productName}) {
    log("WebApiProviderImplementation init");
    _token = token;
    _workspace = workspace;

    _options =
        BaseOptions(contentType: Headers.jsonContentType, baseUrl: baseUrl);

    _dio = Dio(_options);
    //FIXME: remove this instance when this issue will be fixed https://github.com/flutterchina/dio/issues/1308
    _dioRepeat = Dio(_options);

    _dio.interceptors.add(_queuedInterceptorsWrapper());
    _dioRepeat.interceptors.add(_queuedInterceptorsWrapper());

    if (foundation.kDebugMode) {
      _dio.interceptors.add(LogInterceptor(
        requestBody: true,
        request: true,
        requestHeader: true,
        responseHeader: true,
        responseBody: true,
      ));
      _dioRepeat.interceptors.add(LogInterceptor(
        requestBody: true,
        request: true,
        requestHeader: true,
        responseHeader: true,
        responseBody: true,
      ));
    }

    //Setup our ApiStubAdapter to enable the stub mode
    if (isStub) {
      //Setup our ApiStubAdapter to enable the stub mode
      _dio.httpClientAdapter = ApiStubAdapter();
      _dioRepeat.httpClientAdapter = ApiStubAdapter();
    }
  }

  void _updateToken(JWTToken? token) {
    _token = token;
    if (token == null) {
      _workspace = null;
    }
    _jwtTokenStreamController.add(token);
  }

  QueuedInterceptorsWrapper _queuedInterceptorsWrapper() {
    return QueuedInterceptorsWrapper(onRequest:
        (RequestOptions options, RequestInterceptorHandler handler) async {
      final request = options.extra['request'] as Request;
      options.headers["x-app-product"] = productName;

      if (request.authorizationType == AuthorizationType.bearer) {
        // The request requires an authentication

        final String? accessToken = _token?.accessToken;
        if (accessToken != null) {
          // Attempt to use previously saved Access Token
          options.headers["Authorization"] = "Bearer $accessToken";
        }
      }

      if (request.needWorkspaceHeader &&
          _workspace != null &&
          _workspace!.isNotEmpty) {
        options.headers["workspace"] = _workspace;
      }

      return handler.next(options);
    }, onError: (DioException dioError, ErrorInterceptorHandler handler) {
      final Request? request =
          dioError.requestOptions.extra['request'] as Request?;

      if (_checkIfUnauthorizedError(dioError) &&
          request?.authorizationType == AuthorizationType.bearer) {
        final RequestOptions? options = dioError.response?.requestOptions;
        final JWTToken? token = _token;
        if (token != null && options != null) {
          // If the token has been updated, repeat directly.
          if ("Bearer ${token.accessToken}" !=
              options.headers['Authorization']) {
            log('WepApi interceptors: token has been updated, repeat directly.');
            options.headers["Authorization"] = "Bearer ${_token?.accessToken}";
            _dioRepeat.fetch(options).then((r) => handler.resolve(r),
                onError: (e) => handler.reject(e));
            return;
          }

          _refreshToken(token.refreshToken).then((token) {
            log("WepApi interceptors: refresh token success");
            // Refresh Token request complete: token has been refreshed
            _updateToken(token);
            // Set new Bearer
            options.headers["Authorization"] = "Bearer ${token.accessToken}";
          }).then((e) {
            log("WepApi interceptors: repeat request");
            _dioRepeat.fetch(options).then((r) => handler.resolve(r),
                onError: (e) => handler.reject(e));
          }).catchError((error) async {
            // Refresh Token request failed
            log("WepApi interceptors: refreshToken error: $error");
            _updateToken(null);
            handler.reject(dioError);
          });
          return;
        } else {
          // Stored Refresh Token is null, unable to refresh token
          log("WepApi interceptors: session.jWTToken.refreshToken is null force logout");
          _updateToken(null);
          return handler.reject(dioError);
        }
      } else {
        // Handle any other error
        log("WepApi interceptors: not a bearer request, call next error. Current error: ${dioError.message}");
        // Continue to call the next error interceptor
        return handler.next(dioError);
      }
    });
  }

  bool _checkIfUnauthorizedError(DioException dioError) {
    // If the workspace doesn't exist, API return 401,
    // to avoid infinite refresh token loop we need if the check the code value in the response is "UNAUTHORIZED".

    // Sometimes API returns 403 when token is expired,
    // to trigger the refresh token logic, we have to check is the code value in the response is "UNAUTHORIZED".

    if ((dioError.response?.statusCode == 401 ||
            dioError.response?.statusCode == 403) &&
        dioError.response?.data is Map<String, dynamic>) {
      final json = dioError.response?.data as Map<String, dynamic>;
      if (json.isNotEmpty == true && json['code'] == 'UNAUTHORIZED') {
        return true;
      }
    }
    return false;
  }

  Future<JWTToken> _refreshToken(String refreshToken) async {
    final RefreshTokenRequest request = RefreshTokenRequest(refreshToken);
    final defaultHttpClient = Dio(_options);

    if (foundation.kDebugMode) {
      defaultHttpClient.interceptors.add(LogInterceptor(
        requestBody: true,
        request: true,
        requestHeader: true,
        responseHeader: true,
        responseBody: true,
      ));
    }

    if (isStub) {
      // Setup our ApiStubAdapter to enable the stub mode
      defaultHttpClient.httpClientAdapter = ApiStubAdapter();
    }

    return await _performRequest(request, defaultHttpClient).then((response) {
      final refreshTokenResponse = JWTTokenResponse.parse(response.data);
      return refreshTokenResponse.jwtToken;
    });
  }

  @override
  Future<WorkspaceSettings> workspaceSettings() async {
    final request = GetWorkspaceSettingsRequest();
    return await _performRequest(request).then((response) {
      return WorkspaceSettings.fromJson(response.data);
    });
  }

  @override
  Future<PaginatedData<Group>> fetchGroupsPaginated(
      QueryParam params, bool isPoi) async {
    final request = GetGroupsPaginatedRequest(queryParam: params, isPoi: isPoi);
    return await _performRequest(request).then((response) {
      Map<String, dynamic> json = response.data;
      final List<dynamic> results = json["results"];
      final List<Group> items =
          results.map((e) => Group.fromAPIJson(e)).toList();
      return PaginatedData.fromJson(json: json, items: items);
    });
  }

  @override
  Future<PaginatedData<Device>> fetchDevicesPaginated(QueryParam params) async {
    final request = GetDevicesPaginatedRequest(queryParam: params);
    return await _performRequest(request).then((response) {
      Map<String, dynamic> json = response.data;
      final List<dynamic> results = json["results"];
      final List<Device> items =
          results.map((e) => Device.fromDevicesJson(e)).toList();
      return PaginatedData.fromJson(json: json, items: items);
    });
  }

  @override
  Future<Group> fetchGroup(String groupId) async {
    final request = GetGroupRequest(groupId);
    return await _performRequest(request).then((response) {
      return GroupResponse.parse(response.data).group;
    });
  }

  @override
  Future<List<ConnectivityMessage>> fetchConnectivityMessages(
      {required String deviceReference,
      String? deviceToMaintainReference}) async {
    final request = GetConnectivityMessagesRequest(
        deviceReference: deviceReference,
        deviceToMaintainReference: deviceToMaintainReference);
    return await _performRequest(request).then((response) {
      return ConnectivityResponse.parse(response.data['messages']).messages;
    });
  }

  @override
  Future<Report> validateDeviceIBC({required String deviceReference}) async {
    final request = ValidateDeviceRequest(deviceReference: deviceReference);
    return await _performRequest(request).then((response) {
      return Report.fromJson(response.data);
    });
  }

  @override
  Future<PaginatedData<Device>> fetchDashboard(
      {required QueryParam queryParam, Filters? filters}) async {
    final request =
        GetDashboardRequest(queryParam: queryParam, filters: filters);
    return await _performRequest(request).then((response) {
      Map<String, dynamic> json = response.data;
      final List<dynamic> results = json["results"];
      final List<Device> items =
          results.map((e) => Device.fromDashboardJson(e)).toList();
      // FIXME: remove this when the server will filter device with parent combined device
      // items.retainWhere((element) => element.hasParentCombinedDevice != true);
      return PaginatedData.fromJson(json: json, items: items);
    });
  }

  @override
  Future<Device?> fetchDashboardDevice({required String deviceId}) async {
    final request = GetDashboardDeviceRequest(deviceId: deviceId);
    return await _performRequest(request).then((response) {
      // This WS return empty array if device doesn't exist for existing workspace instead of 404
      return (response.data as List<dynamic>).isNotEmpty
          ? Device.fromDashboardJson(response.data[0])
          : null;
    });
  }

  @override
  Future<List<Device>> fetchGroupDevices(String groupId) async {
    final request = GetGroupDevicesRequest(groupId);
    return await _performRequest(request).then((response) {
      return GetDevicesResponse.parse(response.data['devices']).items;
    });
  }

  @override
  Future<List<GraphData>> getGraphData(
      {List<String>? deviceIds,
      List<String>? groupIds,
      required List<String> dataTypes,
      required bool isLastValue,
      DateTime? from,
      DateTime? to}) async {
    final request = GetGraphDataRequest(
        deviceIds: deviceIds,
        groupIds: groupIds,
        dataTypes: dataTypes,
        isLastValue: isLastValue,
        from: from,
        to: to);
    return await _performRequest(request).then((response) {
      return GetGraphDataResponse.parse(response.data["data"]).data;
    });
  }

  @override
  Future<List<Accessory>> fetchGroupAccessories(String groupId) async {
    final request = GetGroupAccessoriesRequest(groupId);
    return await _performRequest(request).then((response) {
      return GetGroupAccessoriesResponse.parse(response.data['results']).items;
    });
  }

  @override
  Future<Accessory> createAccessory(
      {required String groupId,
      required String accessoryId,
      required AccessoryType accessoryType}) async {
    final request = CreateAccessoryRequest(groupId, accessoryId, accessoryType);
    return await _performRequest(request).then((response) {
      return Accessory.fromJson((response.data as List<dynamic>).first);
    });
  }

  @override
  Future<void> reinstallDevice(
      {required String deviceToMaintainReference,
      required String newDeviceReference,
      required DeviceOperationType deviceOperationType}) async {
    await _performRequest(ReinstallDeviceRequest(
        deviceToMaintainReference: deviceToMaintainReference,
        newDeviceReference: newDeviceReference,
        deviceOperationType: deviceOperationType));
  }

  @override
  Future<void> maintainingDevice(
      {required String deviceToMaintainReference,
      required DeviceOperationType deviceOperationType,
      String? comment}) async {
    await _performRequest(MaintenanceDeviceRequest(
        deviceToMaintainReference: deviceToMaintainReference,
        operationType: deviceOperationType,
        comment: comment));
  }

  @override
  Future<void> deleteAccessory(int accessoryId) async {
    final request = DeleteAccessoryRequest(accessoryId);
    await Future.value(_performRequest(request));
  }

  @override
  Future<Group> updateGroup(
    final String groupId,
    final String? name,
    final String? customId,
    final String? postalAddress,
    final String? zipCode,
    final String? town,
    final String? comment,
  ) async {
    final request = UpdateGroupRequest(
        groupId: groupId,
        name: name,
        customId: customId,
        street: postalAddress,
        zipCode: zipCode,
        comment: comment,
        town: town);
    return await _performRequest(request).then((response) {
      return Group.fromAPIJson(response.data);
    });
  }

  @override
  Future<Group> createGroup(
      final String name,
      final String? customId,
      final String? postalAddress,
      final String? zipCode,
      final String? town,
      final String? comment,
      final String? ownerEmail,
      final List<String> orderRecipients) async {
    final request = CreateGroupRequest(
        name: name,
        customId: customId,
        street: postalAddress,
        zipCode: zipCode,
        town: town,
        comment: comment,
        ownerEmail: ownerEmail,
        orderRecipients: orderRecipients);
    return await _performRequest(request).then((response) {
      return Group.fromAPIJson(response.data);
    });
  }

  @override
  Future<Device> getDevice(String deviceId) async {
    final request = GetDeviceRequest(deviceId);
    return await _performRequest(request).then((response) {
      return Device.fromDevicesJson(response.data);
    });
  }

  @override
  Future<void> deleteDevice(String deviceId) async {
    final request = DeleteDeviceRequest(deviceId);
    await Future.value(_performRequest(request));
  }

  @override
  Future<void> deleteGroup(String groupId) async {
    final request = DeleteGroupRequest(groupId);
    await Future.value(_performRequest(request));
  }

  @override
  Future<List<Role>> getUserRoles() async {
    final request = GetWorkspaceRolesRequest();
    return await _performRequest(request).then((response) {
      return GetWorkspaceRolesResponse.parse(response.data['results']).items;
    });
  }

  @override
  Future<List<Role>> getGroupRoles() async {
    final request = GetGroupRolesRequest();
    return await _performRequest(request).then((response) {
      return GetGroupRolesResponse.parse(response.data['results']).items;
    });
  }

  @override
  Future<User> invite(
      {required String email,
      String? firstName,
      String? lastName,
      required List<String> groupIds,
      required Role role,
      required Locale locale}) async {
    final request = InviteRequest(
        email: email,
        firstName: firstName,
        lastName: lastName,
        groupIds: groupIds,
        language: locale,
        role: role);
    return await _performRequest(request).then((response) {
      return User.fromJson(response.data);
    });
  }

  @override
  Future<User> reinvite({required String userId}) async {
    final request = ReinviteRequest(userId: userId);
    return await _performRequest(request).then((response) {
      return User.fromJson(response.data);
    });
  }

  @override
  Future<User> getUser(String userId) async {
    final request = GetUserRequest(userId);
    return await _performRequest(request).then((response) {
      return User.fromJson(response.data);
    });
  }

  @override
  Future<PaginatedData<User>> getUsersPaginated(QueryParam params) async {
    final request = GetUsersPaginatedRequest(queryParam: params);
    return await _performRequest(request).then((response) {
      Map<String, dynamic> json = response.data;
      final List<dynamic> results = json["results"];
      final List<User> items = results.map((e) => User.fromJson(e)).toList();
      return PaginatedData(
          data: items,
          page: json["page"],
          pageSize: json["pageSize"],
          pageCount: json["pageCount"]);
    });
  }

  @override
  Future<User> getMe() async {
    final request = GetMeRequest();
    return await _performRequest(request).then((response) {
      return User.fromMeJson(response.data);
    });
  }

  @override
  Future<List<Workspace>> getWorkspaces() async {
    final request = GetWorkspacesRequest();
    return await _performRequest(request).then((response) {
      final user = User.fromMeJson(response.data);
      return user.workspaces ?? [];
    });
  }

  @override
  Future<FrontConfig> getFrontConfig() async {
    final request = GetFrontConfigRequest();
    return await _performRequest(request).then((response) {
      return FrontConfig.fromJson(response.data);
    });
  }

  String _extractCountry(String language) {
    if (language.contains('zh') && language.contains('TW')) {
      return "zh-TW";
    }
    if (language.contains('zh') && language.contains('CN')) {
      return "zh-CN";
    }
    List<String> strings = language.contains('-')
        ? language.split('-')
        : language.contains('_')
            ? language.split('_')
            : [language];
    return strings[0];
  }

  @override
  Future<String> getTermsAndConditions(String language) async {
    final varPath = (_workspace != null && _workspace == "lantmannen")
        ? "lantmannen/en"
        : _extractCountry(language);
    return Future.value(
        "https://nanolike-public.s3.eu-west-1.amazonaws.com/terms_and_conditions/$varPath/terms_and_conditions.html");
  }

  @override
  Future<User> updateUser(
      {required String userId,
      String? firstName,
      String? lastName,
      String? oldPassword,
      String? newPassword,
      bool? notificationPush,
      bool? notificationEmail,
      Locale? preferredLanguage,
      bool? displayForecast,
      bool? displayRemaining,
      bool? displayLastOrder,
      bool? displayQuickActionOrder,
      String? roleId}) async {
    final request = UpdateUserRequest(
        userId: userId,
        firstName: firstName,
        lastName: lastName,
        oldPassword: oldPassword,
        newPassword: newPassword,
        notificationPush: notificationPush,
        notificationEmail: notificationEmail,
        preferredLanguage: preferredLanguage,
        displayForecast: displayForecast,
        displayRemaining: displayRemaining,
        displayLastOrder: displayLastOrder,
        displayQuickActionOrder: displayQuickActionOrder,
        roleId: roleId);
    return await _performRequest(request).then((response) {
      return User.fromJson(response.data);
    });
  }

  @override
  Future<void> deleteAccount(String userId) async {
    final request = DeleteAccountRequest(userId);
    await _performRequest(request);
  }

  @override
  Future<Device> createDevice({required DevicePayload devicePayload}) async {
    final request = CreateDeviceRequest(devicePayload: devicePayload);
    return await _performRequest(request).then((response) {
      return Device.fromDevicesJson(response.data);
    });
  }

  @override
  Future<Device> updateDevice(
      {required String deviceReference,
      required DevicePayload devicePayload}) async {
    final request = UpdateDeviceRequest(
        deviceReference: deviceReference, devicePayload: devicePayload);
    return await _performRequest(request).then((response) {
      return Device.fromDevicesJson(response.data);
    });
  }

  @override
  Future<JWTToken> signIn(
      {required String login,
      required String password,
      String? workspace}) async {
    final request = LoginRequest(login, password, workspace);
    return await _performRequest(request).then((response) {
      _workspace = workspace;
      final tokenResponse = JWTTokenResponse.parse(response.data);
      _token = tokenResponse.jwtToken;
      return tokenResponse.jwtToken;
    });
  }

  @override
  Future<void> register(String token, String password) async {
    final request = CreateRegisterRequest(token: token, password: password);
    await _performRequest(request);
  }

  @override
  Future<String> validateRegisterToken(String token) async {
    final request = GetRegisterRequest(token: token);
    return await _performRequest(request).then((response) {
      final Map<String, dynamic> json = response.data;
      return json["mail"];
    });
  }

  @override
  Future<void> resetPassword(String email, String language) async {
    final request = ResetPasswordRequest(email, language);
    await _performRequest(request);
  }

  @override
  Future<void> createCalibrationLevel(
      {required String deviceId, required num level, DateTime? date}) async {
    final request = CreateCalibrationLevelRequest(
        deviceId: deviceId, level: level, date: date);
    await _performRequest(request);
  }

  @override
  Future<List<Order>> getOrders() async {
    final request = GetOrdersRequest();
    return await _performRequest(request).then((response) {
      return GetOrdersResponse.fromJson(response.data).orders;
    });
  }

  @override
  Future<Order> getOrder(String orderId) async {
    final request = GetOrderRequest(orderId: orderId);
    return await _performRequest(request).then((response) {
      return Order.fromJson(response.data);
    });
  }

  @override
  Future<Order> createOrder(Order order) async {
    final request = CreateOrderRequest(order);
    return await _performRequest(request).then((response) {
      return Order.fromJson(response.data);
    });
  }

  @override
  Future<Order> updateOrder(Order order) async {
    final request = UpdateOrderRequest(order);
    return await _performRequest(request).then((response) {
      return Order.fromJson(response.data);
    });
  }

  @override
  Future<void> createDeviceContent(String deviceContent) async {
    final request = CreateDeviceContentRequest(deviceContent);
    await _performRequest(request);
  }

  @override
  Future<PaginatedData<DeviceContent>> getDeviceContentsPaginated(
      QueryParam params) async {
    final request = GetDeviceContentsPaginatedRequest(queryParam: params);
    return await _performRequest(request).then((response) {
      Map<String, dynamic> json = response.data;
      final List<dynamic> results = json["results"];
      final List<DeviceContent> items =
          results.map((e) => DeviceContent.fromAPIJson(e)).toList();
      return PaginatedData(
          data: items,
          page: json["page"],
          pageSize: json["pageSize"],
          pageCount: json["pageCount"]);
    });
  }

  @override
  Future<PaginatedData<DeviceContentType>> getDeviceContentTypesPaginated(
      QueryParam params) async {
    final request = GetDeviceContentTypesPaginatedRequest(queryParam: params);
    return await _performRequest(request).then((response) {
      Map<String, dynamic> json = response.data;
      final List<dynamic> results = json["results"];
      final List<DeviceContentType> items =
          results.map((e) => DeviceContentType.fromJson(e)).toList();
      return PaginatedData(
          data: items,
          page: json["page"],
          pageSize: json["pageSize"],
          pageCount: json["pageCount"]);
    });
  }

  @override
  Future<PaginatedData<Drug>> getDrugsPaginated(QueryParam params) async {
    final request = GetDrugsPaginatedRequest(queryParam: params);
    return await _performRequest(request).then((response) {
      Map<String, dynamic> json = response.data;
      final List<dynamic> results = json["results"];
      final List<Drug> items = results.map((e) => Drug.fromJson(e)).toList();
      return PaginatedData(
          data: items,
          page: json["page"],
          pageSize: json["pageSize"],
          pageCount: json["pageCount"]);
    });
  }

  @override
  Future<PaginatedData<Supplement>> getSupplementsPaginated(
      QueryParam params) async {
    final request = GetSupplementsPaginatedRequest(queryParam: params);
    return await _performRequest(request).then((response) {
      Map<String, dynamic> json = response.data;
      final List<dynamic> results = json["results"];
      final List<Supplement> items =
          results.map((e) => Supplement.fromJson(e)).toList();
      return PaginatedData(
          data: items,
          page: json["page"],
          pageSize: json["pageSize"],
          pageCount: json["pageCount"]);
    });
  }

  @override
  Future<void> addFirebaseToken(String userId, String token) async {
    final request = CreateFirebaseTokenRequest(userId: userId, token: token);
    await _performRequest(request);
  }

  @override
  Future<void> deleteFirebaseToken(String userId, String token) async {
    final request = DeleteFirebaseTokenRequest(userId: userId, token: token);
    await _performRequest(request);
  }

  @override
  Future<void> createDeviceMedia(String deviceId, File media) async {
    FormData data =
        FormData.fromMap({"file": await MultipartFile.fromFile(media.path)});
    final request = CreateDeviceMediaRequest(deviceId, data);
    await _performRequest(request);
  }

  @override
  Future<void> deleteDeviceMedia(String mediaId) async {
    final request = DeleteDeviceMediaRequest(mediaId);
    await _performRequest(request);
  }

  @override
  Future<List<Media>> getDeviceMedias(String deviceId) async {
    final request = GetDeviceMediasRequest(deviceId);
    return await _performRequest(request).then((response) {
      return GetDeviceMediasResponse.parse(response.data).medias;
    });
  }

  @override
  Future<List<Role>> getMeRoles() async {
    final request = GetMeRolesRequest();
    return await _performRequest(request).then((response) {
      List<dynamic> list = response.data;
      return list.map((e) => Role.fromJson(e)).toList();
    });
  }

  @override
  List<Locale> getLanguages() {
    return [
      const Locale('da'),
      const Locale('de'),
      const Locale('en', 'CA'),
      const Locale('en', 'US'),
      const Locale('en'),
      const Locale('es'),
      const Locale('fr'),
      const Locale('fr', 'CA'),
      const Locale('it'),
      const Locale('pl'),
      const Locale('pt'),
      const Locale('sv'),
      const Locale('ru'),
      const Locale('th'),
      const Locale('vi'),
      const Locale('zh', 'CH'),
      const Locale('zh', 'TW'),
      const Locale('ja'),
    ];
  }

  @override
  Future<PaginatedData<Role>> getMeRolesPaginated(QueryParam params) async {
    final request = GetMeRolesPaginatedRequest(queryParam: params);

    return await _performRequest(request).then((response) {
      Map<String, dynamic> json = response.data;
      final List<dynamic> results = json["results"];
      final List<Role> items = results.map((e) => Role.fromJson(e)).toList();
      return PaginatedData(
          data: items,
          page: json["page"],
          pageSize: json["pageSize"],
          pageCount: json["pageCount"]);
    });
  }

  @override
  Future<void> addUserToGroup({required User user, required group}) async {
    final request =
        CreateUserToGroupRequest(userId: user.id, groupId: group.id);
    await _performRequest(request);
  }

  @override
  Future<void> deleteUserFromGroupMembership(
      {required User user, required GroupMembership groupMembership}) async {
    final request = DeleteUserFromGroupRequest(
        userId: user.id, groupMembershipId: groupMembership.id);
    await _performRequest(request);
  }

  @override
  Future<CustomView> createCustomView(String name, Filters filters) async {
    final request = CreateCustomViewRequest(name: name, filters: filters);
    return await _performRequest(request).then((response) {
      return CustomView.fromJson(response.data);
    });
  }

  @override
  Future<void> deleteCustomView(String customViewId) async {
    final request = DeleteCustomViewRequest(customViewId);
    await _performRequest(request);
  }

  @override
  Future<PaginatedData<CustomView>> getCustomViewsPaginated(
      QueryParam params) async {
    final request = GetCustomViewsPaginatedRequest(queryParam: params);
    return await _performRequest(request).then((response) {
      Map<String, dynamic> json = response.data;
      final List<dynamic> results = json["results"];
      final List<CustomView> items =
          results.map((e) => CustomView.fromJson(e)).toList();
      return PaginatedData(
          data: items,
          page: json["page"],
          pageSize: json["pageSize"],
          pageCount: json["pageCount"]);
    });
  }

  @override
  Future<CustomView> getCustomView(String customViewId) async {
    final request = GetCustomViewRequest(customViewId);
    return await _performRequest(request).then((response) {
      return CustomView.fromJson(response.data);
    });
  }

  @override
  Future<CustomView> updateCustomView(
      String customViewId, String name, Filters filters) async {
    final request = UpdateCustomViewRequest(
        customViewId: customViewId, name: name, filters: filters);
    return await _performRequest(request).then((response) {
      return CustomView.fromJson(response.data);
    });
  }

  @override
  Future<PaginatedData<Calibration>> getCalibrations(
      {required QueryParam params,
      String? deviceReference,
      CalibrationType? type,
      DateTime? start,
      DateTime? end,
      String? groupId}) async {
    final request = GetCalibrationsPaginatedRequest(
        queryParam: params,
        deviceReference: deviceReference,
        type: type,
        start: start,
        end: end,
        groupId: groupId);
    return await _performRequest(request).then((response) {
      Map<String, dynamic> json = response.data;
      final List<dynamic> results = json["results"];
      final List<Calibration> items =
          results.map((e) => Calibration.fromJson(e)).toList();
      return PaginatedData(
          data: items,
          page: json["page"],
          pageSize: json["pageSize"],
          pageCount: json["pageCount"]);
    });
  }

  @override
  Future<PaginatedData<IBCModel>> fetchIBCModels(
      {required QueryParam queryParam}) async {
    final request = GetIBCModelsPaginatedRequest(
      queryParam: queryParam,
    );
    return await _performRequest(request).then((response) {
      Map<String, dynamic> json = response.data;
      final List<dynamic> results = json["results"];
      final List<IBCModel> items =
          results.map((e) => IBCModel.fromJson(e)).toList();
      return PaginatedData(
          data: items,
          page: json["page"],
          pageSize: json["pageSize"],
          pageCount: json["pageCount"]);
    });
  }

  Future<Response> _performRequest(Request request, [Dio? httpClient]) async {
    final safeHttpClient = httpClient ?? _dio;
    final extra = <String, Request>{};
    extra['request'] = request;
    return await safeHttpClient.request(request.path,
        data: request.data,
        queryParameters: request.queryParams,
        options: Options(
            contentType: Headers.jsonContentType,
            method: request.method,
            headers: request.header,
            extra: extra));
  }

  @override
  Stream<JWTToken?> get jwtTokenStream => _jwtTokenStreamController.stream;

  @override
  void logout() {
    _updateToken(null);
    _workspace = null;
  }

  @override
  void updateWorkspaceHeader(String? workspace) {
    _workspace = workspace;
  }
}
