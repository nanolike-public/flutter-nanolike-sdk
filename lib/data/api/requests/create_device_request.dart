import 'package:flutter_nanolike_sdk/data/api/requests/payload/device_payload.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';

class CreateDeviceRequest extends Request {
  final DevicePayload devicePayload;

  CreateDeviceRequest({required this.devicePayload});

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  get data => devicePayload.toJson();

  @override
  String get method => 'POST';

  @override
  String get mock => 'assets/mocks/post_device.json';

  @override
  String get path => '/v1/devices/';

  @override
  Map<String, dynamic>? get header => null;

  @override
  Map<String, dynamic>? get queryParams => null;
}
