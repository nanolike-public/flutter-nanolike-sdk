import 'package:flutter_nanolike_sdk/domain/models/device_content.dart';
import 'package:flutter_nanolike_sdk/domain/models/ibc_model.dart';
import 'package:flutter_nanolike_sdk/domain/models/location.dart';
import 'package:intl/intl.dart';

class DevicePayload {
  final String? deviceReference;
  final String? groupPoiId;
  final String? deviceName;
  final num? capacity;
  final int? siloNumLegs;
  final Location? location;
  final IBCModel? ibcModel;
  final bool? isSilo;
  final DeviceContent? deviceContent;
  final String? serialNumber;
  final DateTime? deviceFabricationDate;
  final String? color;

  DevicePayload(
      {this.deviceReference,
      this.groupPoiId,
      this.deviceName,
      this.capacity,
      this.siloNumLegs,
      this.location,
      this.ibcModel,
      this.isSilo,
      this.deviceContent,
      this.serialNumber,
      this.deviceFabricationDate,
      this.color});

  Map<String, dynamic> toJson() => {
        if (deviceReference?.isNotEmpty ?? false)
          'device_reference': deviceReference,
        if (groupPoiId?.isNotEmpty ?? false) 'group_ids': [groupPoiId],
        if (deviceName?.isNotEmpty ?? false) 'device_name': deviceName,
        if (capacity != null) 'device_capacity': capacity,
        if (siloNumLegs != null && siloNumLegs! > 0)
          'metadata': {'silo_legs': siloNumLegs},
        if (location != null) ...{
          'device_fixed_lat': location!.latitude,
          'device_fixed_lng': location!.longitude,
        },
        if (isSilo != null) 'is_silo': isSilo,
        if (deviceContent != null) 'device_content_id': deviceContent?.id,
        if (serialNumber?.isNotEmpty ?? false) 'serial_number': serialNumber,
        if (deviceFabricationDate != null)
          'device_fabrication_date':
              DateFormat('yyyy-MM-dd').format(deviceFabricationDate!),
        if (color?.isNotEmpty ?? false) 'color': color,
        if (ibcModel != null) 'model': ibcModel?.name,
      };

  DevicePayload copyWith(
      {String? deviceReference,
      String? groupPoiId,
      String? deviceName,
      num? capacity,
      int? siloNumLegs,
      Location? location,
      IBCModel? ibcModel,
      bool? isSilo,
      DeviceContent? deviceContent,
      String? serialNumber,
      DateTime? deviceFabricationDate,
      String? color,
      String? articleCodeIdentifier,
      bool forceNull = false}) {
    return DevicePayload(
        deviceReference: deviceReference ?? this.deviceReference,
        groupPoiId: groupPoiId ?? this.groupPoiId,
        deviceName: deviceName ?? this.deviceName,
        capacity: capacity ?? this.capacity,
        siloNumLegs: siloNumLegs ?? this.siloNumLegs,
        location: location ?? this.location,
        ibcModel: ibcModel ?? this.ibcModel,
        isSilo: isSilo ?? this.isSilo,
        deviceContent: deviceContent ?? this.deviceContent,
        serialNumber: serialNumber ?? this.serialNumber,
        deviceFabricationDate:
            deviceFabricationDate ?? this.deviceFabricationDate,
        color: color ?? (forceNull ? null : this.color));
  }
}
