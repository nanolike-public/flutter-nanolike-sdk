import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/request_paginated.dart';

class GetIBCModelsPaginatedRequest extends RequestPaginated {
  GetIBCModelsPaginatedRequest({super.queryParam});

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  String get mock => 'assets/mocks/ibc_models_paginated.json';

  @override
  String get path => '/v1/workspace/ibc-models';

  @override
  Map<String, dynamic>? get header => null;
}
