import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/request_paginated.dart';

class GetMeRolesPaginatedRequest extends RequestPaginated {
  GetMeRolesPaginatedRequest({super.queryParam});

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  String get mock => 'assets/mocks/roles_paginated.json';

  @override
  String get path => '/v2/me/roles';

  @override
  Map<String, dynamic>? get header => null;
}
