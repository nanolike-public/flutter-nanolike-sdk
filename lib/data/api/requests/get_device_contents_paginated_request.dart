import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/request_paginated.dart';

class GetDeviceContentsPaginatedRequest extends RequestPaginated {
  GetDeviceContentsPaginatedRequest({super.queryParam});

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  String get mock => 'assets/mocks/device_contents_paginated.json';

  @override
  String get path => '/v1/workspace/device-contents';

  @override
  Map<String, dynamic>? get header => null;
}
