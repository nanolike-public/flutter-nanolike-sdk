import 'dart:ui';

import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';

class UpdateUserRequest extends Request {
  final String userId;
  final String? firstName;
  final String? lastName;
  final String? oldPassword;
  final String? newPassword;
  final bool? notificationPush;
  final bool? notificationEmail;
  final Locale? preferredLanguage;
  final bool? displayForecast;
  final bool? displayRemaining;
  final bool? displayLastOrder;
  final bool? displayQuickActionOrder;
  final String? roleId;

  UpdateUserRequest(
      {required this.userId,
      this.firstName,
      this.lastName,
      this.oldPassword,
      this.newPassword,
      this.notificationPush,
      this.notificationEmail,
      this.preferredLanguage,
      this.displayForecast,
      this.displayRemaining,
      this.displayLastOrder,
      this.displayQuickActionOrder,
      this.roleId});

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  get data => {
        if (firstName != null) "first_name": firstName,
        if (lastName != null) "last_name": lastName,
        if (oldPassword != null) "old_password": oldPassword,
        if (newPassword != null) "new_password": newPassword,
        if (notificationPush != null) "notification_push": notificationPush,
        if (notificationEmail != null) "notification_email": notificationEmail,
        if (preferredLanguage != null)
          "preferred_language": preferredLanguage?.toLanguageTag(),
        if (displayForecast != null) "display_forecast": displayForecast,
        if (displayRemaining != null) "display_remaining": displayRemaining,
        if (displayLastOrder != null) "display_last_order": displayLastOrder,
        if (displayQuickActionOrder != null)
          "display_quick_action_order": displayQuickActionOrder,
        if (roleId != null) "idRole": roleId,
      };

  @override
  String get method => 'PATCH';

  @override
  String get mock => 'assets/mocks/user.json';

  @override
  String get path => '/v1/users/$userId';

  @override
  Map<String, dynamic>? get header => null;

  @override
  Map<String, dynamic>? get queryParams => null;
}
