import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';
import 'package:flutter_nanolike_sdk/domain/models/query_param.dart';

abstract class RequestPaginated extends Request {
  final QueryParam queryParam;

  RequestPaginated({this.queryParam = const QueryParam()});

  @override
  Map<String, dynamic>? get queryParams => queryParam.toMap();

  @override
  get data => null;

  @override
  String get method => 'GET';
}
