import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';

class GetCustomViewRequest extends Request {
  final String customViewId;

  GetCustomViewRequest(this.customViewId);

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  get data => null;

  @override
  String get method => 'GET';

  @override
  String get mock => "assets/mocks/custom_view.json";

  @override
  String get path => '/v2/custom-views/$customViewId/';

  @override
  Map<String, dynamic>? get header => null;

  @override
  Map<String, dynamic>? get queryParams => null;
}
