import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';

class DeleteUserFromGroupRequest extends Request {
  final String userId;
  final String groupMembershipId;

  DeleteUserFromGroupRequest(
      {required this.userId, required this.groupMembershipId});

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  get data => null;

  @override
  String get method => 'DELETE';

  @override
  String get mock => 'assets/mocks/empty.json';

  @override
  String get path => '/v1/users/$userId/group-memberships/$groupMembershipId';

  @override
  Map<String, dynamic>? get header => null;

  @override
  Map<String, dynamic>? get queryParams => null;
}
