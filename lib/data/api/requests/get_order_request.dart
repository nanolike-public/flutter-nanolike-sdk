import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';

class GetOrderRequest extends Request {
  final String orderId;

  GetOrderRequest({required this.orderId});

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  get data => null;

  @override
  String get method => 'GET';

  @override
  String get mock => 'assets/mocks/order.json';

  @override
  String get path => '/v1/orders/$orderId';

  @override
  Map<String, dynamic>? get header => null;

  @override
  Map<String, dynamic>? get queryParams => null;
}
