import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/request_paginated.dart';

class GetDeviceContentTypesPaginatedRequest extends RequestPaginated {
  GetDeviceContentTypesPaginatedRequest({super.queryParam});

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  String get mock => 'assets/mocks/device_content_types_paginated.json';

  @override
  String get path => '/v1/workspace/device-content-types';

  @override
  Map<String, dynamic>? get header => null;
}
