import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/request_paginated.dart';

class GetDrugsPaginatedRequest extends RequestPaginated {
  GetDrugsPaginatedRequest({super.queryParam});

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  String get mock => 'assets/mocks/drugs_paginated.json';

  @override
  String get path => '/v2/drugs';

  @override
  Map<String, dynamic>? get header => null;
}
