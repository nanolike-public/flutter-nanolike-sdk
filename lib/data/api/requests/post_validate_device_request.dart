import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';

class ValidateDeviceRequest extends Request {
  final String deviceReference;

  ValidateDeviceRequest({required this.deviceReference});

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  get data => null;

  @override
  String get method => 'POST';

  @override
  String get mock => 'assets/mocks/empty.json';

  @override
  String get path => '/v1/devices/$deviceReference/validate';

  @override
  Map<String, dynamic>? get header => null;

  @override
  Map<String, dynamic>? get queryParams => null;
}
