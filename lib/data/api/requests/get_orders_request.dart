import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';
import 'package:intl/intl.dart';

class GetOrdersRequest extends Request {
  final DateFormat formatter = DateFormat('yyyy-MM-dd');
  final DateTime now = DateTime.now();

  GetOrdersRequest();

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  get data => null;

  @override
  String get method => 'GET';

  @override
  String get mock => 'assets/mocks/orders.json';

  @override
  String get path => '/v1/orders/';

  @override
  Map<String, dynamic>? get header => null;

  @override
  Map<String, dynamic>? get queryParams {
    final Map<String, dynamic> data = <String, dynamic>{};
    data["minDate"] = formatter.format(now.subtract(const Duration(days: 90)));
    data["maxDate"] = formatter.format(now.add(const Duration(days: 365)));
    data["source"] = "order";
    return data;
  }
}
