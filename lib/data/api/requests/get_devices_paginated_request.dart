import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';
import 'package:flutter_nanolike_sdk/data/api/requests/request_paginated.dart';

class GetDevicesPaginatedRequest extends RequestPaginated {
  GetDevicesPaginatedRequest({super.queryParam});

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  String get mock => "assets/mocks/devices_paginated.json";

  @override
  String get path => '/v1/devices/';

  @override
  Map<String, dynamic>? get header => null;
}
