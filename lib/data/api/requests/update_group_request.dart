import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';

class UpdateGroupRequest extends Request {
  final String _groupId;
  final String? _name;
  final String? _customId;
  final String? _street;
  final String? _town;
  final String? _zipCode;
  final String? _comment;

  UpdateGroupRequest(
      {required String groupId,
      required String? name,
      required String? customId,
      required String? street,
      required String? town,
      required String? comment,
      required String? zipCode})
      : _groupId = groupId,
        _name = name,
        _customId = customId,
        _street = street,
        _town = town,
        _comment = comment,
        _zipCode = zipCode;

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  get data => jsonBody(_name, _customId, _street, _town, _zipCode, _comment);

  @override
  String get method => 'PUT';

  @override
  String get mock => 'assets/mocks/post_group.json';

  @override
  String get path => '/v1/groups/$_groupId';

  @override
  Map<String, dynamic>? get header => null;

  @override
  Map<String, dynamic>? get queryParams => null;

  Map<String, dynamic> jsonBody(String? name, String? customId, String? street,
      String? town, String? zipCode, String? comment) {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['group_name'] = name;
    data['client_poi_id'] = customId;
    data['address_zipcode'] = zipCode;
    data['address_town'] = town;
    data['address_street'] = street;
    data['comment'] = comment;
    return data;
  }
}
