import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';

class CreateRegisterRequest extends Request {
  final String token;
  final String password;

  CreateRegisterRequest({required this.token, required this.password});

  @override
  AuthorizationType get authorizationType => AuthorizationType.basic;

  @override
  bool get needWorkspaceHeader => false;

  @override
  get data => {"password": password, "is_terms_accepted": true};

  @override
  String get method => 'POST';

  @override
  String get mock => 'assets/mocks/empty.json';

  @override
  String get path => '/v1/register';

  @override
  Map<String, dynamic>? get header => null;

  @override
  Map<String, dynamic>? get queryParams => {"token": token};
}
