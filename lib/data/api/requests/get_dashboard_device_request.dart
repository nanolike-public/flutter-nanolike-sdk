import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';

class GetDashboardDeviceRequest extends Request {
  final String deviceId;

  GetDashboardDeviceRequest({required this.deviceId});

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  get data => null;

  @override
  String get method => 'GET';

  @override
  String get mock => "assets/mocks/dashboard_device.json";

  @override
  String get path => '/v2/dashboard/$deviceId';

  @override
  Map<String, dynamic>? get header => null;

  @override
  Map<String, dynamic>? get queryParams => null;
}
