import 'package:flutter_nanolike_sdk/data/api/requests/request.dart';

class GetConnectivityMessagesRequest extends Request {
  final String deviceReference;
  final String? deviceToMaintainReference;

  GetConnectivityMessagesRequest(
      {required this.deviceReference, this.deviceToMaintainReference});

  @override
  AuthorizationType get authorizationType => AuthorizationType.bearer;

  @override
  get data => null;

  @override
  String get method => 'GET';

  @override
  String get mock => 'assets/mocks/connectivity.json';

  @override
  String get path => '/v2/devices/$deviceReference/connectivity';

  @override
  Map<String, dynamic>? get header => null;

  @override
  Map<String, dynamic>? get queryParams => {
        if (deviceToMaintainReference != null)
          'device_to_replace_id': deviceToMaintainReference
      };
}
