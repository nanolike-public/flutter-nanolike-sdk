extension DoubleParsing on String {
  double? tryParseDouble() {
    return num.tryParse(replaceAll(",", "."))?.toDouble();
  }
}

extension EmailCheck on String {
  bool isValidEmail() {
    return RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9-]+\.[a-zA-Z]+")
        .hasMatch(this);
  }
}

extension BankHolidayCheck on String {
  bool isValidBankHoliday() {
    return RegExp(
            r'^(?:0?[1-9]|1[0-2])(?:\/)(?:0?[1-9]|[12]\d|3[01])(?:\/)\d{4}$')
        .hasMatch(this);
  }
}

extension FirstOrNull<T> on Iterable<T> {
  T? get firstOrNull => isEmpty ? null : first;

  T? firstWhereOrNull(bool Function(T element) test) {
    for (var element in this) {
      if (test(element)) return element;
    }
    return null;
  }
}
