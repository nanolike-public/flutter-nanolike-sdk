import 'package:flutter_nanolike_sdk/domain/models/filters.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_nanolike_sdk/data/api/api_implementation.dart';
import 'package:flutter_nanolike_sdk/domain/models/session.dart';
import 'package:flutter_nanolike_sdk/domain/models/token.dart';
import 'package:flutter_nanolike_sdk/domain/presenters/session/session_presenter.dart';
import 'package:flutter_nanolike_sdk/domain/presenters/session/session_presenter_implementation.dart';
import 'package:flutter_nanolike_sdk/domain/providers/api_provider.dart';
import 'package:flutter_nanolike_sdk/domain/providers/storage_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  group('ISessionPresenter', () {
    setUpAll(() async {
      TestWidgetsFlutterBinding.ensureInitialized();
      SharedPreferences.setMockInitialValues({});
    });

    test('signIn & logout', () async {
      final IWebApiJWTProvider webApiProvider = WebApiProviderImplementation(
          baseUrl: "",
          token: null,
          workspace: null,
          isStub: true,
          productName: "binconnect");
      final IStorageProvider storageProvider =
          _StorageProviderTestImplementation();
      final ISessionPresenter sessionPresenter =
          await SessionPresenterImplementation.start(
              webApiProvider, storageProvider);

      await sessionPresenter.logout();

      expect(sessionPresenter.currentMe, null);

      final tokenBeforeLogin = await storageProvider.getJWTToken();
      expect(tokenBeforeLogin, null);

      await sessionPresenter.login("antoine.pemeja@nanolike.com", "password");

      await Future.delayed(const Duration(milliseconds: 100), () async {
        expect(
            sessionPresenter.currentMe?.email, "antoine.pemeja@nanolike.com");

        final tokenAfterLogin = await storageProvider.getJWTToken();
        expect(tokenAfterLogin?.accessToken.isNotEmpty, true);
        expect(tokenAfterLogin?.refreshToken.isNotEmpty, true);

        await sessionPresenter.logout();

        final tokenAfterLogout = await storageProvider.getJWTToken();
        expect(tokenAfterLogout, null);

        expect(sessionPresenter.currentMe, null);
      });
    });
  });
}

class _StorageProviderTestImplementation implements IStorageProvider {
  JWTToken? _jwtToken;
  Session? _session;
  Filters? _filters;

  @override
  Future<JWTToken?> getJWTToken() async {
    return _jwtToken;
  }

  @override
  Future<Session?> getSession() async {
    return _session;
  }

  @override
  Future removeAll() async {
    _jwtToken = null;
    _session = null;
  }

  @override
  Future removeSession() async {
    _session = null;
  }

  @override
  Future saveJWTToken(JWTToken token) async {
    _jwtToken = token;
  }

  @override
  Future saveSession(Session session) async {
    _session = session;
  }

  @override
  Future<Filters?> getFilters() async {
    return _filters;
  }

  @override
  Future saveFilters({Filters? filters}) async {
    _filters = filters;
  }
}
